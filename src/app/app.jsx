//基本组件
import React, {Component} from 'react'
import {render} from 'react-dom'
//自制组件
import Header from '../components/Header'

//页面
import Home from '../pages/Home'
import Login from '../pages/Login'
import Regist from '../pages/Regist'
import Personal from '../pages/Personal'
import Raise from '../pages/Raise'
import Safety from '../pages/Safety'
import Bank from '../pages/Banks'
import LoanDetail from '../pages/LoanDetail'
import Repayment from '../pages/Repayment'
import Loan from '../pages/Loan'

import './whole.scss'
// import { HashRouter, Match, Miss } from 'react-router'
// import { Prompt } from 'react-router'

import {
    // BrowserRouter as Router,
    HashRouter,
    Route,
    Switch
} from 'react-router-dom'

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minHeight: 0
        }
    }

    componentDidMount() {
        this.resize();
        window.onresize = () => this.resize();
    }

    resize() {
        this.setState({
            minHeight: $(window).height() - $('header').height()
        });
    }

    componentDidUpdate() {
        $('header').css('position', 'fixed');
    }

    render() {
        return <HashRouter>
            <section>
                <Header/>
                <div className="body" style={{
                    minHeight: this.state.minHeight
                }}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/login" component={Login}/>
                        <Route exact path="/regist" component={Regist}/>
                        <Route path="/regist/:mobile" component={Regist}/>
                        <Route exact path="/retrieve" component={Regist}/>
                        <Route path="/retrieve/:mobile" component={Regist}/>

                        <Route exact path="/personal" component={Personal}/>
                        <Route path="/personal/:type" component={Personal}/>

                        <Route exact path='/raise' component={Raise}/>
                        <Route exact path='/raise/:type' component={Raise}/>
                        <Route path='/raise/:type/:in' component={Raise}/>

                        <Route exact path='/safety' component={Safety}/>
                        <Route path='/safety/:type' component={Safety}/>

                        <Route path='/bank' component={Bank}/>

                        <Route exact path='/loanDetail' component={LoanDetail}/>
                        <Route exact path='/loanDetail/:loanid' component={LoanDetail}/>
                        <Route path='/loanDetail/:loanid/:plan' component={LoanDetail}/>

                        <Route exact path='/repayment' component={Repayment}/>
                        <Route exact path='/repayment/:type' component={Repayment}/>
                        <Route path='/repayment/ahead/:id' component={Repayment}/>

                        <Route exact path='/loan' component={Loan}/>
                        <Route path='/loan/:type' component={Loan}/>
                        <Route component={Home}/>
                    </Switch>
                </div>
            </section>
        </HashRouter>
        // return  <HashRouter>
        //     <section>
        //       <Header/>
        //       <div className="body" style={{minHeight:this.state.minHeight}}>
        //             <Match exactly pattern='/loan' component={Loan}/>
        //             <Match pattern='/loan/:type' component={Loan}/>
        //             <Miss component={Home}></Miss>
        //       </div>
        //     </section>
        //   </HashRouter>
    }
}

render(
    <App/>, document.getElementById('app'));
