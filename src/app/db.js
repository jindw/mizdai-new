import DBF from './dbFactory'

export default DBF.context;

DBF.create('Place', {
    getPrivince : {
        url       : '/regions/provinces',
    },
    getCity : {
        url       : '/regions/cities',
    },
    getCounty : {
        url       : '/regions/districts',
    }
});

DBF.create('Raise', {
    getBankList : {
        url       : '/bankinfos',
    },
    getRealNameAuth:{
        url       : '/userauth/realnameauth',
        type      : 'POST'
    },
    isRealNameAuthEd:{
        url       : '/userauth/isauthed',
    },
    Companies:{
        url       : '/companies/auth',
        type      : 'POST'
    },
    addContacts:{
        url       : '/contacts',
        type      : 'POST'
    },
    checkbaseauth:{
        url       : '/userauth/checkbaseauth',
    },
    housecontfaractpics:{
        url       : '/housecontfaractpics/auth',
        type      : 'POST'
    },
});

DBF.create('Userextauths', {
    pbc:{
        url       : '/userextauths/pbc',
        type      : 'POST'
    },
    live:{
        url       : '/userextauths/live',
        type      : 'POST'
    },
    marital:{
        url       : '/userextauths/marital',
        type      : 'POST'
    },
    census:{
        url       : '/userextauths/census',
        type      : 'POST'
    },
    education:{
        url       : '/userextauths/education',
        type      : 'POST'
    },
});

DBF.create('Loan', {
    getLoanList : {
        url       : '/loans/user',
        type      : 'GET'
    },
    getLoanDetail : {
        url       : '/loans/detail',
    },
    getLoanPurposes : {
        url       : '/loans/loanpurposes',
    },
    getRepaymentPlans : {
        url       : '/loans/repaymentplans',
    },
    calculateApplyLoan : {
        url       : '/loans/calculateapplyloan',
    },
    calculateRepayments:{
        url       : '/loans/calculaterepayments',
    },
    ifHadSetPaypassword:{
        url       : '/users/issetpaypassword',
    },
    checkpaypassword:{
        url       : '/users/checkpaypassword.json',
        type      : 'POST'
    },
    setPayPassword:{
        url       : '/users/setpaypassword',
        type      : 'POST'
    },
    apply:{
        url       : '/loans/apply',
        type      : 'POST'
    },
});

DBF.create('Banks', {
    getMyBank : {
        url       : '/bankcards/user',
    },
    sendBindvalidateCode: {
        url       : '/bankcards/sendbindvalidatecode',
        type      : 'POST'
    },
    confirm: {
        url       : '/bankcards/confirm',
        type      : 'POST'
    },
    isbankcardbind: {
        url       : '/bankcards/isbankcardbind',
    },
});

DBF.create('RepayMent', {
    getRecord : {
        url       : '/loanbills/repayments',
    },
    outstandingloan : {
        url       : '/loanbills/outstandingloan',
    },
    outstanding: {
        url       : '/loanbills/outstanding',
    },
});

DBF.create('Login', {
    loginNow : {
        url       : '/users/login',
        type      : 'POST'
    }
});

DBF.create('Home', {
    home : {
        url       : '/users/home',
    }
});

DBF.create('Personal', {
    Logout : {
        url       : '/users/logout',
    },
    getUserMsg : {
        url       : '/users',
    },
    getUserauths : {
        url       : '/userauth',
    },
    getUserextauths: {
        url       : '/userextauths',
    },
    applystatus: {
        url       : '/users/applystatus',
        type      : 'POST'
    },
});

DBF.create('Safety', {
    modifypassword : {
        url       : '/users/modifypassword',
        type      : 'POST'
    },
    modifypaypassword : {
        url       : '/users/modifypaypassword',
        type      : 'POST'
    },
    checkrealauth: {
        url       : '/users/checkrealauth',
        type      : 'POST'
    },
});

DBF.create('Regist', {
    sendMessage : {
        url       : '/users/register/sendverifycode',
        type      : 'POST'
    },
    checkvalidatecode:{
        url       : '/users/register/checkvalidatecode',
        type      : 'POST'
    },
    register:{
        url       : '/users/register',
        type      : 'POST'
    },
    checkregisterable:{
        url       : '/users/register/checkregisterable',
    },
    resetpassword : {
        url       : '/users/resetpassword',
        type      : 'POST'
    }
});

DBF.create('UPYun', {
    getSignature : {
        url       : '/upyun/upload',
    },
});