import Cookies from '../components/Cookie'
import _assign from 'lodash/assign'
import _map from 'lodash/map'

export default {
    __ : new Map(),

    set(key, value) {
        this.__.set(key, value);
    },
    get(key) {
        return this.__.get(key);
    },
    create(name, methods) {
        this.context[name] = new DB(methods);
    },
    // 存储db实例
    context : {
        link: data => this.context.Data = data,
        Data: {}
    }
}

let urlPrefix;
if (__LOCAL__) {
    urlPrefix = '//121.40.107.224:8080';
} else {
    urlPrefix = '//sc.mizlicai.com';
}

function DB(methods) {
    _map(methods, (config, method) => this[method] = query => request(config, query));
}

function request(config, querys) {
    const dfd = $.Deferred();
    const mergeQuery = {
        os: 'h5',
        userKey: Cookies.getCookie('userKey')
    };

    $.ajax({
        url: `${urlPrefix}${config.url}.json`,
        type: config.type,
        data: _assign(mergeQuery, querys),
        timeout: 15000
    }).done(resp => {
        if (!resp.status) {
            dfd.resolve(resp);
        } else {
            if (resp.status === 1017) {
                Cookies.clearCookie('userKey');
                location.hash = '#/login';
            }
            dfd.reject(resp);
        }
    }).fail(() => {
        dfd.reject({status: -1, errorMsg: '很遗憾,请求失败,请稍后再试！'});
    });

    return dfd.promise();
}
