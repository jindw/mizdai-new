import React,{Component} from  'react'
import Modal from 'react-modal'

export default class ModalTip extends Component {

    constructor(props) {
        super(props);
        this.state = {
        	tipNum:0
        }
    }

    openModal(){}

    closeModal(){
    	this.setState({
    		tipNum:this.props.tipNum
    	});
    }

    render() {
        return <Modal
                isOpen={this.state.tipNum !== this.props.tipNum}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
            <div className='content result'>
              	<i onClick={this.closeModal.bind(this)}>&times;</i>
              	<p>温馨提示</p>
              	<label>{this.props.msg}</label>
            </div>
            <div className='btn' onClick={this.closeModal.bind(this)}>
                <a href="javascript:;">我知道了</a>
            </div>
        	</Modal>
    }
};