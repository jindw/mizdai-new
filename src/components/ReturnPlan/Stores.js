import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    hidePlan(){
        this.data.cssPlan = 'slideOutDown';
        this.trigger(this.data);
        setTimeout(()=>{
        	this.data.readys = false;
        	this.trigger(this.data);
        },400);
    },

    calculateRepayments(loanAmount,loanCycle,rate,termType){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Loan.calculateRepayments({
            loanAmount:loanAmount,
            loanCycle:loanCycle,
            rate:rate,
            termType:termType,
        }).then(data=>{
            _assign(this.data,data);
            this.data.cssPlan = 'slideInUp';
        	this.data.readys = true;
            this.trigger(this.data);
        }, data=>{
            this.data.modalIsOpen = true;
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    getInitialState() {
        this.data = {
        	repayments:[],
        	readys:false,
        };
        return this.data;
    }
});