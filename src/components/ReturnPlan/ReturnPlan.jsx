import React,{Component} from  'react'
import Loading from '../Loading'
import Modal from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './ReturnPlan.scss'

export default class ReturnPlan extends Component {

    constructor(props) {
        super(props);
        // this.state = {
        //     first:true,
        //     // times:1,
        // }

        sessionStorage.firstTime = 1;
    }

    componentDidUpdate() {
        // if(this.props.times === this.state.times &&this.state.first){
            // console.log(this.props.planState)
            // console.log(this.state.first)
            // console.log(sessionStorage.firstTime)
            // debugger;

        if(this.props.planState && +sessionStorage.firstTime){
        //     // this.setState({first:false});
            sessionStorage.firstTime = 0;
            Actions.calculateRepayments(this.props.loanAmount,
                                        this.props.loanCycle,
                                        this.props.rate,
                                        this.props.termType);
            
        // // }else if(this.props.times !== this.state.times&&!this.state.first){
        // // }else if(!this.props.planState&&!+sessionStorage.firstTime){
        }

        if(this.state.modalIsOpen&&this.props.planState){
            this.props.closePlan();
            sessionStorage.firstTime = 1;
        }
        // else if(!this.props.planState){
        //     console.log(this.props.planState)

        //     // this.setState({first:true});
        //     sessionStorage.firstTime = 1;
        //     this.hidePlan();
        // }

        if(this.state.m.readys){
            // this.setState({first:false});
            const h = $('.returnPlan >div').height() - $('.returnPlan .tops').height() - 0.12*$(window).width();
            $('.returnPlan ul').height(h);
        }
    }

    hidePlan(){
        Actions.hidePlan();
        // this.setState({first:true});
        sessionStorage.firstTime = 1;
        // setTimeout(()=>this.setState({times:++this.state.times}),400);
        this.props.closePlan();
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
        });
        Actions.closeModal();
    }

    render() {
        const [repayments,st] = [[],this.state.m];
        let [allback,moreback] = [0,0];
        st.repayments.forEach((itm,ind)=>{
            allback += itm.amount;
            moreback += itm.interest;
            repayments.push(<li key={`myRepayments_${ind}`}>
                            <em>{itm.number}/{st.repayments.length}</em>
                            还款{itm.amount}元
                            <span>含利息{itm.interest}元</span>
                        </li>);
        });

        return <section style={{display:st.readys?'':'none'}} className='returnPlan'>
                <div style={{height:$(window).height()*0.7}} className={`container animated ${st.cssPlan}`}>
                    <div className='tops' onClick={this.hidePlan.bind(this)}>
                        <i className='timesX'>&times;</i>
                        <label>还款总额</label>
                        <em>{allback}</em>
                        <div>本金{allback - moreback}元
                            <label>利息{moreback}元</label>
                        </div>
                    </div>
                    <ul>
                        {repayments}
                    </ul>
                </div>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                        {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                    </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
            </section>
    }
};

ReactMixin.onClass(ReturnPlan, Reflux.connect(store,'m'));