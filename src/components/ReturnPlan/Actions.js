import Reflux,{createActions} from 'reflux'

export default createActions([
	'calculateRepayments',
	'hidePlan',
	'closeModal',
	]);