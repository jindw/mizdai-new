import React,{Component} from  'react'
import './Loading.scss'

export default class Loading extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <section className={`waiting ${this.props.type}`}>
                <em><i></i></em>
            </section>
    }
};