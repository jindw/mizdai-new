import Arrow_Left from './Svg_Arrow_Left.jsx'
import React,{Component} from 'react'
import _assign from 'lodash/assign'
import './Header.scss'

export default class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '米庄贷',
            backBtn: true,
            rightBtn: false,
        }
    }

    back(){
        if(window.history.length <= 1){
            location.hash = '/#';
            return;
        }

        if(typeof(this.state.backBtn) === 'string'){
            location.hash = this.state.backBtn;
        }else if(history.length < 2){
            location.hash = '/';
        }else{
            history.go(-1);
        }
    }

    reloadPage(){
        window.location.reload();
    }

    componentDidMount(){
        if(!window.__event__) window.__event__= {};
        __event__.setHeader = data => this.setState(_assign(this.state, data));
    }

    toRight(){
        location.hash = this.state.rightBtn[1];
    }

    render() {
        const [title,rightBtn] = [this.state.title,this.state.rightBtn];
        const back_dom = this.state.backBtn ? (<div onClick={this.back.bind(this)}>
                        <Arrow_Left fill='#c7c7cc'/>
                        <div></div>
                    </div>) : <a/>;

        let right_btn;
        if(rightBtn){
            if(rightBtn[2]){
                right_btn = <i onClick={this.toRight.bind(this)} id={rightBtn[2]}></i>
            }else{
                right_btn = <a className='top_right' href="javascript:;" onClick={this.toRight.bind(this)}>{rightBtn[0]}</a>
            }
        }
        
        return <header ref='header' className="header">
                <div className="left">
                    {back_dom}
                </div>
                <div id="js_header_title" className="title">{title}</div>
                <div>
                    {right_btn}
                </div>
            </header>
    }
};