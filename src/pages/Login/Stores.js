import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import Cookies from '../../components/Cookie'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    loginNow(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Login.loginNow(message).then(data=>{
            Cookies.setCookie('userKey',data.user.userKey,30);
            Cookies.setCookie('mobile',message.mobile,300);
            location.hash = '#/';
        }, data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            setTimeout(()=>this.trigger(this.data),300);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
        };
        return this.data;
    }
});