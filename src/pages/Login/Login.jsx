import React,{Component} from  'react'
import Cookies from '../../components/Cookie'
import Modal from 'react-modal'
import ModalWait from 'react-modal'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Login.scss' 

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state={
            username:'',
            minHeight:0,
            defaultMobile:Cookies.getCookie('mobile'),
        }
        if(Cookies.getCookie('userKey')){
            location.hash = '#/';
        }
    }
 
    componentDidMount(){
        __event__.setHeader({
            title: '米庄贷',
            backBtn:false
        });
        scroll(0,0);
        Cookies.clearCookie('validateCode');
        this.setState({
            minHeight:$(window).height() - $('header').height() 
        });
    }

    inputUsername(e){
        e.target.value = e.target.value.replace(/^0+|[^0-9]/g, "").substring(0,11);
    }

    openModal(){}

    closeModal() {
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    toSubmit(){
        const [username,psd] = [this.refs.username.value,this.refs.psd.value];
        let err;
        if(!(/^1[3|4|5|7|8]\d{9}$/.test(username))){
            err = '请输入正确的手机号码';
        }else if(psd.length < 6){
            err = '请输入至少6位密码';
        }

        if(err){
            setTimeout(()=>{
                this.setState({
                    modalIsOpen:true,
                    errmsg:err,
                });
            },300);
        }else{
            Actions.loginNow({
                mobile:username,
                password:psd,
            });
        }
    }

    toEnter(e){
        if(e.which === 13){
            this.toSubmit();
        }
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        setTimeout(()=>$('header').css('position', 'absolute'),200);
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render() {
        const st = this.state.m;
    	return <section className='login' style={{minHeight:this.state.minHeight}}>
                <i></i>
                <div className='username'>
                  	<i></i>
                  	<label>账号</label>
                  	<input id='username' defaultValue={this.state.defaultMobile} 
                    onFocus={this.showClearInput.bind(this,'username')} 
                    onBlur={this.hideClearInput.bind(this,'username')}
                    type="tel" ref='username' 
                    onChange={this.inputUsername.bind(this)}
                    placeholder='请输入您的手机号码'/>
                    <a className='clearInput' onClick={this.clearInput.bind(this,'username')} href="javascript:;">&times;</a>
                </div>
                <div className='password'>
                  	<i></i>
                  	<label>密码</label>
                  	<input onFocus={this.showClearInput.bind(this,'psd')} 
                    onBlur={this.hideClearInput.bind(this,'psd')}
                    onKeyUp={this.toEnter.bind(this)} ref='psd' maxLength='12' type="password" placeholder='请输入登录密码'/>
                    <a className='clearInput' 
                    onClick={this.clearInput.bind(this,'psd')} href="javascript:;">&times;</a>
                </div>
                <a className='find' href="#/retrieve">找回密码</a>
                <a onClick={this.toSubmit.bind(this)} className='loginNow' href="javascript:;">立即登录</a>
                <p>没有账号<a href="#/regist">点击注册</a></p>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={250}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                    {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Login, Reflux.connect(store,'m'));