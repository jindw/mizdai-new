import Reflux,{createActions} from 'reflux'

export default createActions([
	'getImgCode',
	'closeModal',
	'sendRegistCode',
	'checkvalidatecode',
	'register',
	'checkregisterable',
	'closeImgModal',
	'resetpassword',
	]);