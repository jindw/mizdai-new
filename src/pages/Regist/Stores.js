import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import Cookies from '../../components/Cookie'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    resetpassword(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.modalIsOpen = true;
        DB.Regist.resetpassword(message).then(data=>{
            this.data.errorMsg = '操作成功！';
            this.data.over = true;
            this.data.loading = '';
            this.trigger(this.data);
        },data=>{
            this.data.loading = '';
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    checkregisterable(mobile,type){
        this.data.loading = 'on';
        this.trigger(this.data);

        const toThen = data =>{
            if(data.status !== type){
                this.data.modalIsOpen = true;
                this.data.errorMsg = type?'该号码已经注册！':'该号码还未注册';
            }else{
                this.data.modalimgCodeIsOpen = true;
            }
            this.data.loading = '';
            this.trigger(this.data);
        }

        DB.Regist.checkregisterable({mobile:mobile}).then(data=>toThen(data),data=>toThen(data));
    },

    closeImgModal(){
        this.data.modalimgCodeIsOpen = false;
        this.trigger(this.data);
    },

    register(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalIsOpen = true;
        DB.Regist.register(message).then(data=>{
            this.data.errorMsg = '注册成功！';
            this.data.over = true;
            Cookies.setCookie('mobile',message.mobile,300);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    checkvalidatecode(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Regist.checkvalidatecode(message).then(data=>{
            Cookies.setCookie('validateCode',message.validateCode,1)
            location.hash = `#${message.pathname}/${message.mobile}`
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    sendRegistCode(message){
        this.data.loading = 'on';
        this.data.modalimgCodeIsOpen = false;
        this.trigger(this.data);
        this.data.loading = '';
        DB.Regist.sendMessage(message).then(data=>{
            this.data.sendCodeWait = false;
            this.data.codeOk = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.sendCodeWait = true;
            this.data.codeOk = false;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            codeOk:false,
            over:false,
            modalimgCodeIsOpen:false,
        };
        return this.data;
    }
});