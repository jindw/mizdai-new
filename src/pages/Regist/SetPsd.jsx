import React,{Component} from  'react'
import Cookies from '../../components/Cookie'
import Modal from 'react-modal'
import _includes from 'lodash/includes'
import Loading from '../../components/Loading'
import _assign from 'lodash/assign'

import Reflux from 'reflux'
import ReactMixin from 'react-mixin'
import Actions from './Actions'
import store from './Stores'

export default class SetPsd extends Component {
    constructor(props) {
        super(props);
        this.state={
            retrieve:_includes(this.props.location.pathname,'retrieve'),
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
    }

    componentDidMount(){
        let title = '注册';
        if(this.state.retrieve){
            title = '找回密码';
        }
        __event__.setHeader({
            title: title,
            backBtn:true
        });
    }

    openModal(){}

    closeModal() {
        if(this.state.m.over){
            location.hash = '#/login';
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    onSubmit(){
    	const [psd,repsd,mobile] = [this.refs.psd.value,this.refs.repsd.value,this.props.match.params.mobile];
    	let err;
    	if(psd.length < 6){
    		err = '密码至少6位';
    	}else if(psd !== repsd){
    		err = '两次密码输入不一致';
    	}

        const message = {
            validateCode:Cookies.getCookie('validateCode'),
            mobile:mobile,
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            })
        }else if(!this.state.retrieve){
            Actions.register(_assign(message,{password:psd}));
        }else{
            Actions.resetpassword(_assign(message,{newPassword:psd}));
        }
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        $('header').css('position', 'absolute');
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render() {
        const st = this.state.m;
    	return <section className='regist'>
                <div key='registNext' className='registNext'>
                <p>请将您的手机号作为登录账号</p>
                    <dl>
                        <dd key='psd'>
                            <label>新密码</label>
                            <input onFocus={this.showClearInput.bind(this,'psd')}
                            onBlur={this.hideClearInput.bind(this,'psd')}
                             ref='psd' placeholder='请输入您的新密码' maxLength='12' type="password"/>
                            <a className='blueClearInput'
                            onClick={this.clearInput.bind(this,'psd')} href="javascript:;">&times;</a>
                        </dd>
                        <dd key='repsd'>
                            <label>确认新密码</label>
                            <input onFocus={this.showClearInput.bind(this,'repsd')}
                            onBlur={this.hideClearInput.bind(this,'repsd')}
                             ref='repsd' placeholder='请再次输入您的新密码' maxLength='12' type="password"/>
                            <a className='blueClearInput'
                            onClick={this.clearInput.bind(this,'repsd')} href="javascript:;">&times;</a>
                        </dd>
                    </dl>
                    <a className={`btn_abs_bottom android_${this.state.anDroid}`}  href="javascript:;" onClick={this.onSubmit.bind(this)}>确定</a>
                </div>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'>
                    <i onClick={this.closeModal.bind(this)}>&times;</i>
                     {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(SetPsd, Reflux.connect(store,'m'));
