import React,{Component} from  'react'
import Modal from 'react-modal'
import ModalImgCode from 'react-modal'
import CountDown from '../../components/CountDown'
import Loading from '../../components/Loading'
import _includes from 'lodash/includes'
import _startsWith from 'lodash/startsWith'

import Reflux from 'reflux'
import ReactMixin from 'react-mixin'
import Actions from './Actions'
import store from './Stores'

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state={
            username:'',
            codeTxt:'发送验证码',
            modalimgCodeIsOpen:false,
            mobile:'',
            time:Date.now(),
            sendMsg:null,
            sendCodeWait:false,
            type:_includes(this.props.location.pathname,'retrieve')?0:1005,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        //1005未注册，0注册了
    }

    componentDidMount(){
        const title = this.state.type?'注册':'找回密码';
        __event__.setHeader({
            title: title,
            backBtn:true
        });

        new Cleave('.username', {
            phone: true,
            phoneRegionCode: 'cn',
        });
    }

    inputUsername(){
        const username = this.refs.username;
        if(!_startsWith(username.value,"+86 ")){
            username.value = '+86 ';
        }
    }

    registFirstNext(){
        const [code,mobile] = [this.refs.code.value,this.refs.username.value.substring(3).replace(/^0+|[^0-9]/g, "")];
        let err;
        if(!(/^1[3|4|5|7|8]\d{9}$/.test(mobile))){
            err = '请输入正确的手机号码';
        }else if(code.length < 4){
            err = '请输入正确的短信验证码';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }else{
            Actions.checkvalidatecode({
                mobile:mobile,
                validateCode:code,
                pathname:this.props.location.pathname,
            });
        }
    }

    openModal(){}

    closeModal() {
        this.setState({
            modalIsOpen:false,
            sendCodeWait:false,
            modalimgCodeIsOpen:this.state.sendCodeWait||this.state.m.sendCodeWait,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    openModalimgCode(){
        this.setState({
            modalimgCodeIsOpen:true,
            time:Date.now(),
        });
    }

    closeModalimgCode() {
        this.changeImgCode();
        this.setState({modalimgCodeIsOpen:false});
        Actions.closeImgModal();
    }

    componentDidUpdate() {
        $('header').css('position', 'absolute');
    }

    findError(){
        const username = this.refs.username.value.substring(3).replace(/^0+|[^0-9]/g, "");
        if(!(/^1[3|4|5|7|8]\d{9}$/.test(username))){
            return '请输入正确的手机号码';
        }
    }

    beforeSendCode(){
        const username = this.refs.username.value.substring(3).replace(/^0+|[^0-9]/g, "");
        if(/^1[3|4|5|7|8]\d{9}$/.test(username)){
            Actions.checkregisterable(username,this.state.type);
            this.setState({mobile:username});
        }else{
            this.setState({
                errmsg:'请输入正确的手机号码',
                modalIsOpen:true,
            });
        }
    }

    changeImgCode(){
        this.setState({time: Date.now()});
    }

    imgCodeDown(){
        const [validateCode,mobile] = [this.refs.validateCode.value,this.state.mobile];
        if(validateCode.length < 4){
            this.setState({
                modalIsOpen:true,
                errmsg:'请输入正确的图形验证码',
                modalimgCodeIsOpen:false,
                sendCodeWait:true
            });
            Actions.closeImgModal();
            return;
        }
        Actions.sendRegistCode({
            mobile:mobile,
            validateCode:validateCode,
        });
        $('#countDown').off('click').on('click',()=> {
            if(!$('#countDown.off').length){
                Actions.sendRegistCode({
                    mobile:mobile,
                    validateCode:validateCode,
                });
            }
        });
        this.setState({modalimgCodeIsOpen: false});
    }

    componentWillUnmount() {
        $('#countDown').off('click');
    }

    focusMobNow(input){
        $('header').css('position', 'absolute');
        if(this.state.m.codeOk){
            $(this.refs.username).blur();
        }else{
            $(this.refs[input]).addClass('showclear');
        }
    }

    clearInput(input){
        this.refs[input].value = input === 'username'?'+86 ':'';
        this.refs[input].focus();
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
    }

    render() {
        const st = this.state.m;

        let urlPrefix;
        if (__LOCAL__) {
            urlPrefix = '//121.40.107.224:8080';
        } else {
            urlPrefix = '//sc.mizlicai.com';
        }

    	return <section className='regist'>
                <div key='registFirst' className='registFirst'>
                <p>请将您的手机号作为登录账号</p>
                    <dl>
                        <dd key='mobile'>
                            <label>手机号码</label>
                            <input defaultValue='+86' maxLength = '20' className='username'
                                onBlur={this.hideClearInput.bind(this,'username')}
                                onFocus={this.focusMobNow.bind(this,'username')}
                                onChange={this.inputUsername.bind(this)}
                            ref='username' placeholder='请输入您的手机号码' type="tel"/>
                            <a className='blueClearInput'
                            onClick={this.clearInput.bind(this,'username')} href="javascript:;">&times;</a>
                        </dd>
                        <dd key='code'>
                            <a style={{display:(st.codeOk?'none':'')}} onClick={this.beforeSendCode.bind(this)}
                            href="javascript:;" id='beforeSendCode'></a>
                            <label>验证码</label>
                            <input ref='code' placeholder='验证码' maxLength='4' type="tel"/>
                            <CountDown sendNow = {st.codeOk} time='60' />
                        </dd>
                    </dl>
                </div>
                <a href="javascript:;"
                    className={`btn_abs_bottom android_${this.state.anDroid}`}
                    onClick={this.registFirstNext.bind(this)}>
                    下一步
                </a>
                <ModalImgCode
                    isOpen={st.modalimgCodeIsOpen||this.state.modalimgCodeIsOpen}
                    onAfterOpen={this.openModalimgCode.bind(this)}
                    onRequestClose={this.closeModalimgCode.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content imgcode'>
                        <i onClick={this.closeModalimgCode.bind(this)}>&times;</i>
                        <p>图形验证码</p>
                        <div>
                            <input ref='validateCode' placeholder='请输入图形验证码' maxLength='4' type="text"/>
                            <img onClick={this.changeImgCode.bind(this)} src={`${urlPrefix}/validatecodes/randcode.json?mobile=${this.state.mobile}&time=${this.state.time}`}/>
                        </div>
                    </div>
                  <div className='btn'>
                      <a href="javascript:;" onClick={this.imgCodeDown.bind(this)}>确定</a>
                  </div>
                </ModalImgCode>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                     {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Main, Reflux.connect(store,'m'));
