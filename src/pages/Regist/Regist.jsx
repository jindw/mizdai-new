import React,{Component} from  'react'
import Main from './Main.jsx'
import SetPsd from './SetPsd.jsx'
import './Regist.scss'

export default class Regist extends Component {
    constructor(props) {
        super(props);
    }

    render() {
      // console.log(this.props.match.params)
      const params = this.props.match.params;
        if(!params.mobile){
            return <Main {...this.props}/>
        }
        return <SetPsd {...this.props}/>
   }
};
