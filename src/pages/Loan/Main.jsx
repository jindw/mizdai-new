import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import ModalUse from 'react-modal'
import ModalTime from 'react-modal'
import ModalPsd from 'react-modal'
import Cookies from '../../components/Cookie'
import Loading from '../../components/Loading'
import ReturnPlan from '../../components/ReturnPlan'
import _times from 'lodash/times'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Main.scss'

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            modalUseIsOpen:false,
            useful:'',
            modalTimeIsOpen:false,
            time:'',
            psdlen:0,
            firstpsd:'',
            cssPlan:'',
            showPlan:false,
            lilv:0,
            termType:0,
            modalPsdIsOpen:false,
            modalPsdIsOpenWait:false,
            mypsd:'',
            times:0,
        }
        Actions.getLoanPurposes();
        Actions.getRepaymentPlans();
        Actions.getMyBank();
        Actions.home();
    }

    componentDidMount() {
	     __event__.setHeader({
            title: '申请借款',
            backBtn:true,
            raiseRight:false,
        });
	}

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        if(this.state.m.toConfirm){
            location.hash = 'loan/confirm';
        }
        this.setState({
            modalIsOpen:false,
            modalPsdIsOpen:this.state.modalPsdIsOpenWait||this.state.m.modalPsdIsOpenWait,
            modalPsdIsOpenWait:false,
        });
        Actions.closeModal();
        Actions.closeModalPsdIsOpenWait();
    }

    openModalUse(){
        this.setState({modalUseIsOpen:true});
    }

    closeModalUse() {
        this.setState({modalUseIsOpen:false});
    }

    openModalTime(){
        this.setState({modalTimeIsOpen:true});
    }

    closeModalTime() {
        this.setState({modalTimeIsOpen:false});
    }

    openModalPsd(){
        const psd = this.state.mypsd;
        this.refs.password.value = psd;
        this.setState({psdlen:psd.length});
        this.inPassword();
    }

    closeModalPsd(){
        this.setState({
            modalPsdIsOpen:false,
            psdlen:0,
            mypsd:'',
            firstpsd:'',
        });
        Actions.closePsdModal();
    }

    useSelect(use){
        setTimeout(()=>{
            this.setState({
                modalUseIsOpen:false,
                useful:use.name,
                useid:use.id,
            })
        },300);
    }

    timeSelect(time){
        this.setState({
            modalTimeIsOpen:false,
            time:time,
            lilv:+time.termType === 2?time.loanRatio:time.loanRatio*time.loanCycle,
            termType:+time.termType,
            timeId:time.id,
            loanCycle:+time.termType === 2?time.loanCycle:1,
        });
        this.loanInMoney(time.id);
    }

    inPassword(){
        this.refs.password.focus();
    }

    psdKey(){
        const password = this.refs.password;
        const mypsd = password.value.replace(/[^0-9]/g, "");
        password.value = mypsd;
        this.setState({
            psdlen:mypsd.length,
            mypsd:mypsd,
        })
    }

    psdNext(){
        const psd = this.refs.password.value;
        if(psd.length < 6){
            this.setState({
                modalIsOpen:true,
                errmsg:'请输入6位数字密码',
                modalPsdIsOpen:false,
                modalPsdIsOpenWait:true,
            });
            Actions.closePsdModal();
        }else if(this.state.m.newpsd && !this.state.firstpsd){
            Actions.newPsdLast();
            this.setState({
                firstpsd:psd,
                psdlen:0,
            });
            this.refs.password.value = '';
            this.inPassword();
        }else if(this.state.firstpsd){
            if(psd === this.state.firstpsd){
                this.setState({modalPsdIsOpen:false});
                Actions.setPayPassword({payPassword:psd});
            }else{
                this.setState({
                    modalIsOpen:true,
                    errmsg:'两次密码输入不一致',
                    modalPsdIsOpen:false,
                    modalPsdIsOpenWait:true,
                });
                Actions.closePsdModal();
            }
        }
    }

    toNext(){
        const [val] = [this.refs.val.value];
        let err;
        if(!this.state.useful){
            err = '请选择借款用途';
        }else if(val % 1000 || val <= 0){
            err = '请输入正确的借款金额(1000的整数倍)';
        }else if(!this.state.time){
            err = '请选择借款期限';
        }else if(this.state.m.avaiAmount < val){
            err = '申请借款金额超过可借款金额';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            })
        }else{
            Actions.ifHadSetPaypassword(val);

            Cookies.setCookie('amount',val,1);
            Cookies.setCookie('bankCardId',this.state.m.bankCard.id,1);
            Cookies.setCookie('purposeId',this.state.useid,1);
            Cookies.setCookie('repaymentPlanId',this.state.timeId,1);

            Cookies.setCookie('fee',this.state.m.fee,1);
            Cookies.setCookie('loanPlanName',this.state.time.loanPlanName,1);
            Cookies.setCookie('feilv',(this.state.lilv*100).toFixed(2),1);
            Cookies.setCookie('myBankName',this.refs.myBankName.textContent,1);
            Cookies.setCookie('interest',this.state.m.interest,1);
            Cookies.setCookie('termType',this.state.termType,1);
        }
    }

    showPlan(){
        this.setState({times:++this.state.times});
    }

    loanInMoney(planIds){
        const [val,planId] = [this.refs.val,planIds>0?planIds:this.state.time.id];
        val.value = val.value.replace(/^0+|[^0-9]/g, "");
        this.setState({val:val.value});
        if(val.value&&planId){
            Actions.calculateApplyLoan(val.value,planId);
        }
    }

    openUse(){
        setTimeout(()=>this.openModalUse(),300);
    }

    openTime(){
        setTimeout(()=>this.openModalTime(),300);
    }

    render() {
        const [st,loanPurposes,repaymentPlans,repayments] = [this.state.m,[],[],[]];
        st.loanPurposes.forEach((itm,ind)=>loanPurposes.push(<dd key={`loanPurposes_${ind}`} onClick={this.useSelect.bind(this,itm)}>{itm.name}</dd>));
    	st.repaymentPlans.forEach((itm,ind)=>repaymentPlans.push(<dd key={`repaymentPlans${ind}`} onClick={this.timeSelect.bind(this,itm)}>{itm.loanPlanName}</dd>));
        
        let [allback,moreback] = [0,0];
        st.repayments.forEach((itm,ind)=>{
            allback += itm.amount;
            moreback += itm.interest;
            repayments.push(<dd key={`repayments_${ind}`}>
                            <em>{itm.number}/{st.repayments.length}</em>
                            还款{itm.amount}元
                            <span>含利息{itm.interest}元</span>
                        </dd>);
        });

        let psdMod = [];
        _times(6,(ind)=>psdMod.push(<dd key={`psdMod_${ind}`} className={this.state.psdlen > ind ?'on':''}><i/></dd>));
        
        return <section className='loan'>
    		<dl className='rightSvg'>
    			<dd onClick={this.openUse.bind(this)}>借款用途
                    <Arrow fill='#3366cc'/>
                    <span className={this.state.useful?'':'none'}>{this.state.useful||'请选择'}</span>
                </dd>
    			<dd className='hasinput'>借款金额
                    <input maxLength='7' onBlur={this.loanInMoney.bind(this)} 
                    ref='val' placeholder='请选择1000的整数倍' type="tel"/><span>元</span>
                </dd>
    			<dd onClick={this.openTime.bind(this)}>借款期限
                    <Arrow fill='#3366cc'/>
                    <span className={this.state.time?'':'none'}>{this.state.time.loanPlanName||'请选择'}</span>
                </dd>
    			<dd>利率(按月计息)<span>{(this.state.lilv*100).toFixed(2)}%</span></dd>
    			<dd>手续费<span>{st.fee||0}元</span></dd>
    			<dd style={{display:(this.state.termType === 2 ? '':'none')}}>每月利息<span>{st.installmentInterest||0}元</span></dd>
    			<dd>实际到款<span>{st.realPayAmount||0}元</span></dd>
    			<dd style={{display:(this.refs.val&&this.refs.val.value&&this.state.time&&this.state.lilv&&this.state.termType?'':'none')}} onClick={this.showPlan.bind(this)}>还款计划<Arrow fill='#3366cc'/></dd>
    			<dd>收款银行<span ref='myBankName'>{st.bankCard.bankName||'-'}(尾号{(st.bankCard.cardNumberOriginal||'****').substring((st.bankCard.cardNumberOriginal||'****').length-4)})</span></dd>
    		</dl>
    		<a onClick={this.toNext.bind(this)} className='btn_abs_bottom' href="javascript:;">下一步</a>
             
            <ReturnPlan {...this.state} loanAmount = {this.state.val} rate = {this.state.lilv}/>

            <ModalPsd
                isOpen={st.modalPsdIsOpen||this.state.modalPsdIsOpen}
                onAfterOpen={this.openModalPsd.bind(this)}
                onRequestClose={this.closeModalPsd.bind(this)}
                closeTimeoutMS={150}
                style={{
                    content:{
                        overflow:'hidden',
                    }
                }}
            >
                <div className='content' id='password'>
                    <i className='timesX' onClick={this.closeModalPsd.bind(this)}>&times;</i>
                    密码设置
                </div>
                <div>
                    <p>{st.passwordTip}</p>
                    <dl className='password_confirm' onClick={this.inPassword.bind(this)}>
                        {psdMod}
                    </dl>
                    <input autoFocus onKeyUp={this.psdKey.bind(this)} ref='password' maxLength="6" type="tel"/>
                    <a onClick={this.psdNext.bind(this)} href="javascript:;">下一步</a>
                </div>
            </ModalPsd>

            <ModalTime
                isOpen={this.state.modalTimeIsOpen}
                onAfterOpen={this.openModalTime.bind(this)}
                onRequestClose={this.closeModalTime.bind(this)}
                closeTimeoutMS={150}
            >
                <div className='content select'>
                    <i onClick={this.closeModalTime.bind(this)}>&times;</i>请选择借款期限
                </div>
                <dl>
                    {repaymentPlans}
                </dl>
            </ModalTime>

            <ModalUse
                isOpen={this.state.modalUseIsOpen}
                onAfterOpen={this.openModalUse.bind(this)}
                onRequestClose={this.closeModalUse.bind(this)}
                closeTimeoutMS={150}
            >
                <div className='content select'>
                      <i onClick={this.closeModalUse.bind(this)}>&times;</i>请选择借款用途
                  </div>
                  <dl>
                    {loanPurposes}
                  </dl>
            </ModalUse>
            <Modal
                isOpen={st.modalIsOpen||this.state.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
                <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                    {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                </div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Main, Reflux.connect(store,'m'));