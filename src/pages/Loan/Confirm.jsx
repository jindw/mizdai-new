import React,{Component} from  'react'
import ModalPsd from 'react-modal'
import Modal from 'react-modal'
import Cookies from '../../components/Cookie'
import Loading from '../../components/Loading'
import LoanAgreeMent from './LoanAgreeMent.jsx'
import ModalAgreeMent from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Confirm.scss'

export default class Confirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
        	modalPsdIsOpen:false,
            mypsd:'',
            agree:true,
            modalAgreeMentIsOpen:false,
        }
        if(!Cookies.getCookie('userKey')){
            location.hash = '/';
        }
    }

    componentDidMount() {
	    __event__.setHeader({
            title: '申请借款',
            backBtn:true,
        });
	}

    openModalPsd(){
        const psd = this.state.mypsd;
        this.refs.password.value = psd;
        this.setState({psdlen:psd.length});
        this.inPassword();
    }

    inPassword(){
        this.refs.password.focus();
    }

    closeModalPsd(){
        this.setState({
            modalPsdIsOpen:false,
            mypsd:'',
        });
    }

    psdKey(){
        const password = this.refs.password;
        const mypsd = password.value.replace(/[^0-9]/g, "");
        password.value = mypsd;
        this.setState({
            psdlen:mypsd.length,
            mypsd:mypsd,
        })
    }

    psdNext(){
        const psd = this.refs.password.value;
        if(psd.length < 6){
            this.setState({
                modalIsOpen:true,
                errmsg:'请输入6位数字密码',
                modalPsdIsOpen:false,
                modalPsdIsOpenWait:true,
            });
            Actions.closePsdModal();
        }else{//支付
            Actions.applys({
                amount:Cookies.getCookie('amount'),
                bankCardId:Cookies.getCookie('bankCardId'),
                payPassword:psd,
                purposeId:Cookies.getCookie('purposeId'),
                repaymentPlanId:Cookies.getCookie('repaymentPlanId'),
            });
            this.closeModalPsd();
        }
    }

    toConfirm(){
        this.setState({modalPsdIsOpen:true});
    }

    
    openModalAgreeMent(){
        this.setState({
            modalAgreeMentIsOpen: true,
        });
    }
    closeModalAgreeMent(){
        this.setState({
            modalAgreeMentIsOpen: false,
        });
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.toMain){
            location.hash = '#';
        }
        this.setState({
            modalIsOpen:false,
            modalPsdIsOpen:this.state.modalPsdIsOpenWait||this.state.m.modalPsdIsOpenWait,
            modalPsdIsOpenWait:false,
            errmsg:this.state.m.modalPsdIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    toAgree(){
        this.setState({agree:!this.state.agree});
    }

    render() {
        const st = this.state.m;

        let psdMod = [];
        for (let i in [1,2,3,4,5,6]) {
            psdMod.push(<dd key={`psdMod_${i}`} className={this.state.psdlen > i ?'on':''}><i/></dd>);
        }

    	return <section className='loan confirm'>
    		<dl>
    			<dd>借款金额<span>{Cookies.getCookie('amount')}元</span></dd>
    			<dd>应还利息<span>{Cookies.getCookie('interest')}元</span></dd>
    			<dd>手续费<span>{Cookies.getCookie('fee')}元</span></dd>
    			<dd>借款期数<span>{Cookies.getCookie('loanPlanName')}</span></dd>
    			<dd>利率(按月计息)<span>{Cookies.getCookie('feilv')}%</span></dd>
    			<dd>收款银行<span>{Cookies.getCookie('myBankName')}</span></dd>
    		</dl>
            <p id='box_check'>
                <i onClick = {this.toAgree.bind(this)} className={this.state.agree?'on':''}></i>
                我已阅读并同意 <a href="javascript:;" onClick={this.openModalAgreeMent.bind(this)}>《米庄贷借款协议》</a>
            </p>
    		<a onClick={this.toConfirm.bind(this)} className='btn_abs_bottom' href="javascript:;">确定</a>
            
            <ModalAgreeMent
                isOpen={this.state.modalAgreeMentIsOpen}
                onAfterOpen={this.openModalAgreeMent.bind(this)}
                onRequestClose={this.closeModalAgreeMent.bind(this)}
                closeTimeoutMS={150}
                style={{
                    content:{
                        width:'95%',
                    }
                }}
            >   <section onClick = {this.closeModalAgreeMent.bind(this)}>
                    <LoanAgreeMent/>
                </section>
            </ModalAgreeMent>
            <ModalPsd
                isOpen={st.modalPsdIsOpen||this.state.modalPsdIsOpen}
                onAfterOpen={this.openModalPsd.bind(this)}
                onRequestClose={this.closeModalPsd.bind(this)}
                closeTimeoutMS={150}
            >
                <div className='content' id='password'>
                    <i className='timesX' onClick={this.closeModalPsd.bind(this)}>&times;</i>
                    请输入支付密码
                </div>
                <div>
                    <p>{st.passwordTip}</p>
                    <dl className='password_confirm' onClick={this.inPassword.bind(this)}>
                        {psdMod}
                    </dl>
                    <input onKeyUp={this.psdKey.bind(this)} ref='password' maxLength="6" type="tel"/>
                    <a onClick={this.psdNext.bind(this)} href="javascript:;">下一步</a>
                </div>
            </ModalPsd>
            <Modal
                isOpen={st.modalIsOpen||this.state.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
                <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                    {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                </div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <Loading type={st.loading}/>
    	</section>
   }
};

ReactMixin.onClass(Confirm, Reflux.connect(store,'m'));