import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserMsg(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Personal.getUserMsg().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    home(){
        DB.Home.home().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    getMyBank(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        const t = this;
        class isbankcardbind {
            constructor(bankCardId) {
                DB.Banks.isbankcardbind({
                    bankCardId
                }).then(data=>{
                    if(data.isBind){
                        _assign(t.data,data);
                        t.trigger(t.data);
                    }else{
                        location.hash = '#/raise';
                    }
                },data=>{
                    _assign(t.data,data);
                    t.data.modalIsOpen = true;
                    t.trigger(t.data);
                });
            }
        }

        DB.Banks.getMyBank().then(data=>{
            new isbankcardbind(data.bankCard.id);
            _assign(this.data,data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    applys(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalPsdIsOpen = false;
        DB.Loan.apply(message).then(data=>{
            location.hash = '#/loan/success';
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    inputPsd(){
        this.data.modalPsdIsOpen = true;
        this.data.passwordTip = '请输入您的支付密码';
        this.trigger(this.data);
    },

    setPayPassword(message){
        this.data.modalPsdIsOpen = false;
        this.data.modalIsOpen = true;

        DB.Loan.setPayPassword(_assign(message,{validateCode:this.data.validateCode})).then(data=>{
            _assign(this.data,data);
            this.data.errorMsg = '密码设置成功';
            this.data.modalPsdIsOpenWait = false;
            this.data.toConfirm = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalPsdIsOpenWait = true;
            this.trigger(this.data);
        });
    },

    newPsdStart(){
        this.data.modalPsdIsOpen = true;
        this.data.passwordTip = '请设置您的支付密码';
        this.trigger(this.data);
    },

    newPsdLast(){
        this.data.modalPsdIsOpen = true;
        this.data.passwordTip = '请再次输入您的支付密码';
        this.trigger(this.data);
    },

    ifHadSetPaypassword(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Loan.ifHadSetPaypassword().then(data=>{
            _assign(this.data,data);
            this.data.newpsd = !data.isSetPayPassword;
            if(!data.isSetPayPassword){
                this.newPsdStart();
            }else{
                location.hash = '#/loan/confirm';
            }
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    getLoanPurposes(){
    	DB.Loan.getLoanPurposes().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
    	},data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    getRepaymentPlans(){
        DB.Loan.getRepaymentPlans().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    calculateApplyLoan(loanAmount,repaymentPlanId){
        DB.Loan.calculateApplyLoan({
            loanAmount,
            repaymentPlanId,
        }).then(data=>{
            this.data.installmentInterest = 0;
            _assign(this.data,data);
            this.trigger(this.data);
        }, data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        })
    },

    hidePlan(){
        this.data.showPlan = false;
        this.trigger(this.data);
    },

    calculateRepayments(loanAmount,loanCycle,rate,termType){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Loan.calculateRepayments({
            loanAmount,
            loanCycle,
            rate,
            termType,
        }).then(data=>{
            _assign(this.data,data);
            this.data.showPlan = true;
            this.trigger(this.data);
        }, data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    closeModalPsdIsOpenWait(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    closeModal(){
        this.data.modalPsdIsOpenWait = false;
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    closePsdModal(){
        this.data.modalPsdIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            loanPurposes:[],
            repaymentPlans:[],
            break:false,
            modalIsOpen:false,
            fee:0,
            repayments:[],
            modalPsdIsOpen:false,
            bankCard:{},
            loading:'',
            user:{},
        };
        return this.data;
    }
});