import Reflux,{createActions} from 'reflux'

export default createActions(
	['getLoanPurposes',
	'getRepaymentPlans',
	'calculateApplyLoan',
	'calculateRepayments',
	'ifHadSetPaypassword',
	'closePsdModal',
	'newPsdLast',
	'setPayPassword',
	'closeModal',
	'closeModalPsdIsOpenWait',
	'inputPsd',
	'applys',
	'getMyBank',
	'hidePlan',
	'home',
	'getUserMsg',
	]);