import React,{Component} from  'react'
import Main from './Main.jsx'
import Confirm from './Confirm.jsx'
import Success from './Success.jsx'


export default class Loan extends Component {
    constructor(props) {
        super(props);
    }

    render() {
    	switch(this.props.match.params.type){
    		case 'confirm':
    			return <Confirm/>;
    		case 'success':
    			return <Success/>
    		default:
    			return <Main/>
    	}
   }
}
