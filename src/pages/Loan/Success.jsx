import React,{Component} from  'react'
import './Success.scss'

export default class Success extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
	     __event__.setHeader({
            title: '借款成功',
            backBtn:'#/',
        });
	}

    render() {
    	return <section className='loan_success'>
    		<div>
    			<p>	
    				<label>提交成功，您申请的借款将于<br/>审核通过后24小时内打款至您的银行卡</label>
    				<a href="#/">返回首页</a>
    			</p>
    		</div>
    	</section>
   }
}