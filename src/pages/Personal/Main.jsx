import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Main.scss'

export default class Main extends Component {

    constructor(props) {
        super(props);
        Actions.getUserMsg();
        this.state = {
            minHeight:0,
        }
    }

    componentDidMount(){
        __event__.setHeader({
            title: '个人中心',
            backBtn:true
        });

        this.setState({
            minHeight:$(window).height() - $('header').height(),
        });
    }

	toMe(){
		location.hash = '#/personal/me';
	}

    toSafety(){
        location.hash = '#/safety';
    }

    toBank(){
        location.hash = '#/bank';
    }

    logOut(){
        Actions.logout();
    }

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        Actions.closeModal();
    }

    toService(){
        location.hash = '#/personal/service'
    }

    render() {
        const st = this.state.m;
       	return <section className='personal' style={{minHeight:this.state.minHeight}}>
                <div className='top'>
                	<i></i>
                	<p>ID:{st.user.mobile}</p>
                </div>
                <dl className='rightSvg'>
                	<dd onClick={this.toMe}><i></i><div>个人信息</div><Arrow fill='#3366cc'/></dd>
                	<dd onClick={this.toBank}><i></i><div>银行卡</div><Arrow fill='#3366cc'/></dd>
                	<dd onClick={this.toSafety}><i></i><div>账户安全</div><Arrow fill='#3366cc'/></dd>
                	<dd onClick={this.toService}><i></i><div>客户服务</div><Arrow fill='#3366cc'/></dd>
                	<dd style={{display:'none'}}><i></i><div>常见问题</div><Arrow fill='#3366cc'/></dd>
                </dl>
                <p>当前版本v3.0.0</p>
                <a href="javascript:;" onClick={this.logOut}>退出登录</a>
                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errorMsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Main, Reflux.connect(store,'m'));