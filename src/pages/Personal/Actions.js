import Reflux,{createActions} from 'reflux'

export default createActions(['logout','getUserMsg','closeModal']);