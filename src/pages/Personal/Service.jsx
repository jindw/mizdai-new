import React,{Component} from  'react'
import Modal from 'react-modal'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Service.scss'

export default class Service extends Component {

    constructor(props) {
        super(props);
        Actions.getUserMsg();
    }

	componentDidMount() {
	     __event__.setHeader({
            title: '客户服务',
            backBtn:true,
        });
	}

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        Actions.closeModal();
    }

    render() {
        const st = this.state.m;
       	return <section className='personal service'>
                <div className='me'>
                	<div className='top'>
                		<p>400-699-8883</p>
                		<i className='tel'><a href='tel:4006998883'/></i>
                	</div>
                	<div className='kefu rightSvg'>
                		<i></i>
                		<div>
                			在线客服
                			<label>工作时间：9:00-21:00</label>
                		</div>
                		<Arrow fill='#3366cc'/>
                	</div>
                	<dl>
                		<dt>微信订阅号&nbsp;&nbsp;烟庄金融</dt>
                		{/*<dd>
	                		<label>客服QQ群</label>
	                		<ul>
	                			<li>1群：454433868 (已满)</li>
	                			<li>2群：343896724 (已满)</li>
	                			<li>3群：156118371</li>
	                		</ul>
                		</dd>*/}
                	</dl>
                </div>
                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errorMsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(Service, Reflux.connect(store,'m'));
