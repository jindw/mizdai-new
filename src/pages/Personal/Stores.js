import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import Cookies from '../../components/Cookie'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserMsg(){
        this.data.loading = '';
        DB.Personal.getUserMsg().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    logout(){
    	DB.Personal.Logout().then(data=>{
            Cookies.clearCookie('userKey');
            location.hash = '#/login';
    	},data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            bankCard:[],
            user:{},
            loading:'on',
        };
        return this.data;
    }
});