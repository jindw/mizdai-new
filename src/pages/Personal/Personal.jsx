import React,{Component} from  'react'
import Reflux from 'reflux'
import Main from './Main.jsx'
import Me from './Me.jsx'
import Service from './Service.jsx';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
      const params = this.props.match.params;
        if(params.type === 'me'){
            return <Me/>
        }else if(params.type === 'service'){
            return <Service/>
        }else{
            return <Main/>
        }
   }
}

export default App;
