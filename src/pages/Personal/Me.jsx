import React,{Component} from  'react'
import Modal from 'react-modal'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

export default class Me extends Component {

    constructor(props) {
        super(props);
        Actions.getUserMsg();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '个人信息',
            backBtn:true
        });
    }

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        Actions.closeModal();
    }

    render() {
        const st = this.state.m;
        let sex = st.user.idCard?(st.user.idCard.substring(16,17)%2?'男':'女'):'不详';
       	return <section className='personal'>
                <div className='me'>
                	<div className='top'>
                		<i></i>
                		<p>ID:{st.user.mobile||'-'}</p>
                	</div>
                	<dl>
                		<dd>姓名<span>{st.user.realName||'-'}</span></dd>
                		<dd>性别<span>{sex}</span></dd>
                		<dd>身份证号<span className='number'>{st.user.idCard||'-'}</span></dd>
                	</dl>
                </div>
                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errorMsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Me, Reflux.connect(store,'m'));