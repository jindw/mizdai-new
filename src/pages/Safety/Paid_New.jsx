import React,{Component} from  'react'
import Modal from 'react-modal'
import _times from 'lodash/times'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Paid_New.scss'

export default class Paid_New extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            tip:'请设置您的交易密码',
            minHeight:0,
            first:null,
        }
        Actions.ifHadSetPaypassword();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '交易密码',
            backBtn:true,
        });
        this.setState({
        	minHeight:$(window).height() - $('header').height() + 'px',
        });
        this.inPassword();
        const mypsd = this.refs.mypsd;
        $(mypsd).css('height', $(mypsd).width()/6);
    }

    Operate(op){
        location.hash = `#/safety/${op}`;
    }

    inPassword(){
        this.refs.password.focus();
    }

    psdKey(){
        const password = this.refs.password;
        const mypsd = password.value.replace(/[^0-9]/g, "");
        password.value = mypsd;
        this.setState({
            psdlen:mypsd.length,
            mypsd:mypsd
        })
    }

    psdNext(){
        const psd = this.refs.password.value;
        if(psd.length < 6){
            this.setState({
                modalIsOpen:true,
                errmsg:'请输入6位数字密码',
            });
        }else if(!this.state.first){//第一次输入
            this.setState({
            	tip:'请再次输入您的交易密码',
            	first:psd,
                psdlen:0,
            });
            this.refs.password.value = '';
        }else if(psd !== this.state.first){//2
            this.setState({
                modalIsOpen:true,
                errmsg:'两次密码输入不一致',
                psdlen:0,
            });
            this.refs.password.value = '';
        }else{ 
            Actions.setPayPassword({payPassword:psd});
        }
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    componentDidUpdate() {
        if(!this.state.modalIsOpen){
            this.inPassword();
        }
    }

    render() {
        const [st,psdMod] = [this.state.m,[]];
        _times(6,ind=>psdMod.push(<dd key={`psdMod_${ind}`} className={this.state.psdlen > ind ?'on':''}><i/></dd>));

       	return <section className='paid_new' style={{minHeight:this.state.minHeight}}>
                <p>{this.state.tip}</p>
                <dl ref='mypsd' className='password_confirm' onClick={this.inPassword.bind(this)}>
                    {psdMod}
                </dl>
                <input onKeyUp={this.psdKey.bind(this)} ref='password' maxLength="6" type="tel"/>
                <a onClick={this.psdNext.bind(this)} href="javascript:;">下一步</a>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(Paid_New, Reflux.connect(store,'m'));