import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import CountDown from '../../components/CountDown'
import Loading from '../../components/Loading'
import _includes from 'lodash/includes'
import _times from 'lodash/times'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Paid_Find.scss'

class First extends Component {

    constructor(props) {
        super(props);
        Actions.hideLoading();
        this.state = {
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
    }

    toNext(){
        const [name,idcard] = [this.refs.name.value,this.refs.idcard.value];
        let err;
        if(!name){
            err ='请输入姓名';
        }else if(!idcard){
            err ='请输入身份证号';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            })
        }else{
            Actions.checkrealauth({
                name:name,
                idCard:idcard,
            });
        }
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        $('header').css('position', 'absolute');
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render(){
        const st = this.state.m;
        return <section className='findpaid first'>
            <p>为了保障您的账户安全，在找回密码前<br/>需先验证您的身份信息</p>
            <dl>
                <dd>
                    <label>姓名</label>
                    <input onFocus={this.showClearInput.bind(this,'name')}
                        onBlur={this.hideClearInput.bind(this,'name')}
                     maxLength='10' ref='name' type="text" placeholder='请输入姓名'/>
                    <a className='blueClearInput'
                    onClick={this.clearInput.bind(this,'name')} href="javascript:;">&times;</a>
                </dd>
                <dd>
                    <label>身份证号</label>
                    <input onFocus={this.showClearInput.bind(this,'idcard')}
                        onBlur={this.hideClearInput.bind(this,'idcard')}
                         maxLength='18' ref='idcard' type="text" placeholder='请输入身份证号'/>
                    <a className='blueClearInput'
                    onClick={this.clearInput.bind(this,'idcard')} href="javascript:;">&times;</a>
                </dd>
            </dl>
            <a onClick={this.toNext.bind(this)} className={`btn_abs_bottom android_${this.state.anDroid}`}
             href="javascript:;">下一步</a>
            <Modal
                isOpen={st.modalIsOpen||this.state.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
              <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
              </div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <Loading type={st.loading}/>
        </section>
    }
};

class Second extends Component {

    constructor(props) {
        super(props);
        this.state = {
            time:Date.now(),
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        Actions.getUserMsg();
    }

    toNext(){
        const [validateCode,code] = [this.refs.validateCode.value,this.refs.code.value];
        let err;
        if(!validateCode){
            err ='请输入图形验证码';
        }else if(!code){
            err ='请输入短信验证码';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            })
        }else{
            Actions.checkvalidatecode(this.state.m.user.mobileOri,code);
        }
    }

    openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    changeImgCode(){
        this.setState({time:Date.now()});
    }

    componentDidMount() {
        const cd = $('#countDown');
        cd.on('click', ()=> {
            const validateCode = this.refs.validateCode.value;
            if(validateCode.length < 4){
                this.setState({
                    modalIsOpen:true,
                    errmsg:'请输入正确的图形验证码！',
                });
            }else{
                if(!_includes($('#countDown').text(),'(')){
                    Actions.sendRegistCode({
                        mobile:this.state.m.user.mobileOri,
                        validateCode:validateCode,
                    });
                }
            }
        });
    }

    render(){
        const st = this.state.m;

        let urlPrefix;
        if (__LOCAL__) {
            urlPrefix = '//121.40.107.224:8080';
        } else {
            urlPrefix = '//sc.mizlicai.com';
        }

        return <section className='findpaid first' id='second'>
            <p>发送验证码短信（免费）到号码<br/>{st.user.mobileOri||'-'}</p>
            <dl>
                <dd>
                    <label>图形验证码</label>
                    <input ref='validateCode' maxLength='4' type="text" placeholder='图形验证码'/>
                    <img style={{display:(st.user.mobileOri?'':'none')}}
                        onClick={this.changeImgCode.bind(this)}
                        src={`${urlPrefix}/validatecodes/randcode.json?mobile=${st.user.mobileOri}&time=${this.state.time}`}/>
                </dd>
                <dd>
                    <label>验证码</label>
                    <input maxLength='4' ref='code' type="text" placeholder='请输入验证码'/>
                    <CountDown sendNow = {st.codeOk} time='60' />
                </dd>
            </dl>
            <a onClick={this.toNext.bind(this)} className={`btn_abs_bottom android_${this.state.anDroid}`}
             href="javascript:;">完成</a>
            <Modal
                isOpen={st.modalIsOpen||this.state.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
              <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
              </div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <Loading type={st.loading}/>
        </section>
    }
};

class Last extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            tip:'请设置6位数字交易密码',
            minHeight:0,
            first:null,
        }
        Actions.stepThree();
    }

    componentDidMount(){
        this.setState({
            minHeight:$(window).height() - $('header').height() + 'px',
        });
        this.inPassword();
        const mypsd = this.refs.mypsd;
        $(mypsd).css('height', $(mypsd).width()/6);
    }

    Operate(op){
        location.hash = `#/safety/${op}`;
    }

    inPassword(){
        this.refs.password.focus();
    }

    psdKey(){
        const password = this.refs.password;
        const mypsd = password.value.replace(/[^0-9]/g, "");
        password.value = mypsd;
        this.setState({
            psdlen:mypsd.length,
            mypsd:mypsd
        })
    }

    psdNext(){
        const psd = this.refs.password.value;
        if(psd.length < 6){
            this.setState({
                modalIsOpen:true,
                errmsg:'请输入6位数字密码',
            });
        }else if(!this.state.first){//第一次输入
            this.setState({
                tip:'请再次输入您的交易密码',
                first:psd,
                psdlen:0,
            });
            this.refs.password.value = '';
        }else if(psd !== this.state.first){//2
            this.setState({
                modalIsOpen:true,
                errmsg:'两次密码输入不一致',
                psdlen:0,
            });
            this.refs.password.value = '';
        }else{
            Actions.setPayPassword({
                payPassword:psd,
            });
        }
    }

    openModal(){
        this.setState({modalIsOpen:true})
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    componentDidUpdate() {
        if(!this.state.modalIsOpen){
            this.inPassword();
        }
    }

    render() {
        const [st,psdMod] = [this.state.m,[]];
        _times(6,ind=>psdMod.push(<dd key={`psdMod_${ind}`} className={this.state.psdlen > ind ?'on':''}><i/></dd>));
        return <section className='paid_new' style={{minHeight:this.state.minHeight}}>
                <p>{this.state.tip}</p>
                <dl ref='mypsd' className='password_confirm' onClick={this.inPassword.bind(this)}>
                    {psdMod}
                </dl>
                <input onKeyUp={this.psdKey.bind(this)} ref='password' maxLength="6" type="tel"/>
                <a onClick={this.psdNext.bind(this)} href="javascript:;">下一步</a>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

export default class Paid_Find extends Component{

    componentDidMount() {
        __event__.setHeader({
            title: '找回交易密码',
            backBtn:true,
        });
    }

    render(){
        if(this.state.m.padiFindStep === 1){
            return <First/>
        }else if(this.state.m.padiFindStep === 2){
            return <Second/>
        }else{
            return <Last/>
        }
    }
}

ReactMixin.onClass(Paid_Find, Reflux.connect(store,'m'));
ReactMixin.onClass(Last, Reflux.connect(store,'m'));
ReactMixin.onClass(First, Reflux.connect(store,'m'));
ReactMixin.onClass(Second, Reflux.connect(store,'m'));
