import Reflux,{createActions} from 'reflux'

export default createActions([
	'modifypassword',
	'closeModal',
	'ifHadSetPaypassword',
	'setPayPassword',
	'modifypaypassword',
	'checkrealauth',
	'isRealNameAuthEd',
	'getUserMsg',
	'sendRegistCode',
	'checkvalidatecode',
	'hideLoading',
	'checkpaypassword',
	'padiModifyStart',
	'stepThree',
	'hadSetPaypasswordCheck',
	'applystatus',
	]);