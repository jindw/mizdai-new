import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

export default class PaidOperate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minHeight:0,
        }
        Actions.ifHadSetPaypassword();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '交易密码管理',
            backBtn:true,
        });

        this.setState({
            minHeight:$(window).height() - $('header').height() + 'px',
        });
    }
 
    Operate(op){
        if(this.state.m.isSetPayPassword){
            location.hash = `#/safety/${op}`;
        }else{
            location.hash = '#/safety/newpaid';
        }
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.base === false){
            location.hash = "#/raise";
        }
        
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    render() {
        const st = this.state.m;
       	return <section className='safety' style={{minHeight:this.state.minHeight}}>
                <dl className='main paid'>
                	<dd onClick={this.Operate.bind(this,'modifypaid')}>
                        <div>修改交易密码</div><Arrow fill='#3366cc'/>
                    </dd>
                	<dd onClick={this.Operate.bind(this,'findpaid')}>
                        <div>找回交易密码</div><Arrow fill='#3366cc'/>
                    </dd>
                </dl>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(PaidOperate, Reflux.connect(store,'m'));