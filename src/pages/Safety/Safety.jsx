import React,{Component} from  'react'
import Main from './Main.jsx'
import Operate from './Operate.jsx'
import PaidOperate from './PaidOperate.jsx';
import Paid_New from './Paid_New.jsx'
import Paid_Modify from './Paid_Modify.jsx'
import Paid_Find from './Paid_Find.jsx'
import './Safety.scss'

export default class Safety extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        switch(this.props.match.params.type){
            case 'login':
                return <Operate/>;
            case 'paid':
                return <PaidOperate/>;
            case 'newpaid':
                return <Paid_New/>
            case 'modifypaid':
                return <Paid_Modify/>
            case 'findpaid':
                return <Paid_Find/>
            default:
                return <Main/>
        }
   }
};
