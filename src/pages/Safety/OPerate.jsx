import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import Cookies from '../../components/Cookie'
import Loading from '../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

export default class Operate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        if(!Cookies.getCookie('userKey')){
            location.hash = '#/login';
        }
        Actions.hideLoading();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '修改登录密码',
            backBtn:true,
        });
    }

	toMe(){
		location.hash = '#/personal/me';
	}

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            Cookies.clearCookie('userKey');
            location.hash = '#/login';
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    toSubmit(){
        const[oldpsd,newpsd,renewpsd] = [this.refs.oldpsd.value,this.refs.newpsd.value,this.refs.renewpsd.value];
        let err;
        if(!oldpsd){
            err = '请输入当前密码！';
        }else if(newpsd.length < 6){
            err='新密码至少六位数！';
        }else if(!renewpsd){
            err='请重复输入新密码！';
        }else if(newpsd !== renewpsd){
            err='两次密码输入不一致！'
        }else if(oldpsd === newpsd){
            err='新旧密码重复！';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            })
        }else{
            Actions.modifypassword({
                oldPassword:oldpsd,
                newPassword:newpsd,
            });
        }
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        $('header').css('position', 'absolute');
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render() {
        const st = this.state.m;

       	return <section className='safety'>
                <dl className='operate' id='operate'>
                	<dd>
                        <span>当前密码</span>
                        <input onFocus={this.showClearInput.bind(this,'oldpsd')} 
                            onBlur={this.hideClearInput.bind(this,'oldpsd')} 
                            ref='oldpsd' maxLength ='12' placeholder='请输入当前密码'
                        type="password"/>
                        <a className='blueClearInput' 
                        onClick={this.clearInput.bind(this,'oldpsd')} href="javascript:;">&times;</a>
                    </dd>
                	<dd>
                        <span>新密码</span>
                        <input onFocus={this.showClearInput.bind(this,'newpsd')} 
                            onBlur={this.hideClearInput.bind(this,'newpsd')} 
                             ref='newpsd' maxLength = '12' placeholder='请输入新密码' type="password"/>
                        <a className='blueClearInput' 
                            onClick={this.clearInput.bind(this,'newpsd')} href="javascript:;">&times;</a>
                    </dd>
                	<dd>
                        <span>确认新密码</span>
                        <input onFocus={this.showClearInput.bind(this,'renewpsd')} 
                            onBlur={this.hideClearInput.bind(this,'renewpsd')} 
                             ref='renewpsd' maxLength = '12' placeholder='请再次输入新密码' type="password"/>
                        <a className='blueClearInput' 
                            onClick={this.clearInput.bind(this,'renewpsd')} href="javascript:;">&times;</a>
                    </dd>
                </dl>
                <a className={`btn_abs_bottom android_${this.state.anDroid}`}  href="javascript:;" onClick={this.toSubmit.bind(this)}>确定</a>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Operate, Reflux.connect(store,'m'));