import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

export default class Paid_Modify extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            tip:'请输入原交易密码',
            minHeight:0,
            first:null,
            oldpsd:null,
        }
        Actions.hideLoading();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '修改交易密码',
            backBtn:true,
        });
        this.setState({
        	minHeight:$(window).height() - $('header').height() + 'px',
        });
        const mypsd = this.refs.mypsd;
        $(mypsd).css('height', $(mypsd).width()/6);
        // this.inPassword();
    }

    Operate(op){
        location.hash = `#/safety/${op}`;
    }

    inPassword(){
        $(this.refs.password).focus();
    }

    psdKey(){
        const password = this.refs.password;
        const mypsd = password.value.replace(/[^0-9]/g, "");
        password.value = mypsd;
        this.setState({
            psdlen:mypsd.length,
            mypsd:mypsd
        });
        if(mypsd.length === 6){
            $(password).blur();
        }
    }

    psdNext(){
        const psd = this.refs.password.value;
        if(psd.length < 6){
            this.setState({
                modalIsOpen:true,
                errmsg:'请输入6位数字密码',
            });
        }else if(!this.state.oldpsd){
            Actions.checkpaypassword(psd);
        }else if(!this.state.first){//第一次输入
            if(this.state.oldpsd === psd){
                this.setState({
                    psdlen:0,
                    modalIsOpen:true,
                    errmsg:'新密码和旧密码相同！'
                });
            }else{
                this.setState({
                    tip:'请再次输入您的交易密码',
                    first:psd,
                    psdlen:0,
                });
            }
            this.refs.password.value = '';
        }else if(psd !== this.state.first){//2
            this.setState({
                modalIsOpen:true,
                errmsg:'两次密码输入不一致',
                psdlen:0,
            });
            this.refs.password.value = '';
        }else{
            Actions.modifypaypassword({newPassword:psd,oldPassword:this.state.oldpsd});
        }
    }

    openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
        if(this.state.m.padiModifyFail){
            this.setState({
                psdlen:0,
            });
            this.refs.password.value = '';
            Actions.padiModifyStart();
        }
    }

    componentDidUpdate() {
        if(!this.state.modalIsOpen){
            this.inPassword();
        }

        if(this.state.m.padiModify&&!this.state.oldpsd){
            let psd = this.refs.password;
            this.setState({
                tip:'请输入您的新交易密码',
                oldpsd:psd.value,
                psdlen:0,
            });
            psd.value = '';
        }
    }

    render() {
        const st = this.state.m;
        let psdMod = [];
        for (let i in [1,2,3,4,5,6]) {
            psdMod.push(<dd key={`psdMod_${i}`} className={this.state.psdlen > i ?'on':''}><i/></dd>);
        }
       	return <section className='paid_new' style={{minHeight:this.state.minHeight}}>
                <p>{this.state.tip}</p>
                <dl className='password_confirm' ref='mypsd' onClick={this.inPassword.bind(this)}>
                    {psdMod}
                </dl>
                <input onKeyUp={this.psdKey.bind(this)} ref='password' maxLength="6" type="tel" autoFocus/>
                <a onClick={this.psdNext.bind(this)} href="javascript:;">下一步</a>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Paid_Modify, Reflux.connect(store,'m'));