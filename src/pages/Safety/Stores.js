import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    applystatus(){
        DB.Personal.applystatus().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    setPayPassword(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalIsOpen = true;
        DB.Loan.setPayPassword(_assign(message,{
            validateCode:localStorage.validateCode
        })).then(data=>{
            _assign(this.data,data);
            this.data.errorMsg = '密码设置成功';
            this.data.break = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    stepThree(){
        this.data.padiFindStep = 3;
        this.trigger(this.data);
    },

    padiModifyStart(){
        this.data.padiModifyFail = false;
        this.trigger(this.data);
    },

    checkpaypassword(payPassword){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Loan.checkpaypassword({payPassword:payPassword}).then(data=>{
            this.data.padiModify = true;
            this.data.padiModifyFail = false;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.padiModifyFail = true;
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    checkvalidatecode(mobile,validateCode){
        this.data.loading = 'on';
        this.trigger(this.data);
        localStorage.validateCode = validateCode;
        this.data.loading = '';
        DB.Regist.checkvalidatecode({
            mobile:mobile,
            validateCode:validateCode,
        }).then(data=>{
            this.data.padiFindStep = 3;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    sendRegistCode(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        DB.Regist.sendMessage(message).then(data=>{
            this.data.codeOk = true;
            this.data.loading = '';
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.codeOk = false;
            this.data.loading = '';
            this.trigger(this.data);
        });
    },

    getUserMsg(){
        this.data.padiFindStep = 2;
        this.data.loading = '';
        DB.Personal.getUserMsg().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    checkrealauth(message){
        DB.Safety.checkrealauth(message).then(data=>{
            this.data.padiFindStep = 2;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    modifypaypassword(message){
        this.data.modalIsOpen = true;
        this.data.padiFindStep = 3;
        DB.Safety.modifypaypassword(message).then(data=>{
            _assign(this.data,data);
            this.data.errorMsg = '密码设置成功';
            this.data.break = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    ifHadSetPaypassword(){
        DB.Loan.ifHadSetPaypassword().then(data=>{
            _assign(this.data,data);
            localStorage.validateCode = data.validateCode;
            this.trigger(this.data);
        },data=>{
            this.data.modalIsOpen = true;
            this.data.errorMsg = '请先完成基础认证！';
            this.data.base = false;
            this.trigger(this.data);
        });
    },

    modifypassword(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalIsOpen = true;
        DB.Safety.modifypassword(message).then(data=>{
            this.data.break = true;
            this.data.errorMsg = '操作成功';
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    hideLoading(){
        this.data.loading = '';
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            loading:'on',
            break:false,
            repayments:[],
            padiFindStep:1,
            user:{},
            padiModify:false,
        };
        return this.data;
    }
});