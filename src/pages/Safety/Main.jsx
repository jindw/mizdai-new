import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minHeight:0,
        }
    }

    componentDidMount(){
        __event__.setHeader({
            title: '账户安全',
            backBtn:true,
        });
        Actions.applystatus();
        
        this.setState({
            minHeight: $(window).height() - $('header').height(),
        });
    }

    changeLogin(){
        location.hash = '#/safety/login';
    }

    changePaid(){
        if(this.state.m.process === 'PASS'){
            location.hash = '#/safety/paid';
        }else{
            this.setState({
                modalIsOpen:true,
                errmsg:'您需要先完成基础认证并审核通过！',
            })
        }
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        this.setState({modalIsOpen:false});
    }

    render() {
       	return <section className='safety' style={{minHeight:this.state.minHeight}}>
                <dl className='main'>
                	<dd onClick={this.changeLogin.bind(this)}>
                        <i className='loginpsd'></i><div>登录密码</div><Arrow fill='#3366cc'/></dd>
                	<dd onClick={this.changePaid.bind(this)}>
                        <i className='paypsd'></i>
                        <div>交易密码</div>
                        <Arrow fill='#3366cc'/>
                    </dd>
                </dl>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(Main, Reflux.connect(store,'m'));