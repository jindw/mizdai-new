import React,{Component} from  'react'
import Modal from 'react-modal'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Banks.scss'

export default class Banks extends Component {
    constructor(props) {
        super(props);
        Actions.getMyBank();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '我的银行卡',
            backBtn:true
        });
    }

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        Actions.closeModal();
    }

    render() {
        const st = this.state.m;
        const myBank = st.bankCard;
        return <section className='banks'>
                <div className={`bank_${myBank.bankCode}`}>
                    <dl>
                        <dt>{myBank.bankName}</dt>
                        <dd>尾号{(myBank.cardNumberOriginal||'****').substring((myBank.cardNumberOriginal||'****').length-4)}</dd>
                    </dl>
                </div>
                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errorMsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Banks, Reflux.connect(store,'m'));