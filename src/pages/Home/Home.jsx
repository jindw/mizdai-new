import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Cookies from '../../components/Cookie'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Home.scss'
import {Motion, spring} from 'react-motion'

class Loged extends Component{
    constructor(props) {
        super(props);
        this.state = {
            minHeight:0,
        };
        Actions.applystatus();
        Actions.home();
        localStorage.raiseRight = false;
    }

    componentDidMount() {
        this.setState({
            minHeight:$(window).height() - $('header').height(),
        });
        __event__.setHeader({
            title: '米庄贷',
            backBtn:false,
        });
    }

    toLoanDetail(){
        location.hash = '#/loandetail';
    }

    toLoan(){
        if(this.state.m.base){
            location.hash='#/loan';
        }else{
            location.hash='#/raise';
        }
    }

    toRaise(){
        location.hash='#/raise';
    }

    toRepayMent(){
        location.hash='#/repayment';
    }

    getLocalNum(num){
        if(num === undefined)return '0.00';
        const [i,re,len] = [`${num}`.indexOf('.'),num.toLocaleString(),`${num}`.length];
        if(i === -1){
            return `${re}.00`;
        }else if(len - i === 2){
            return `${re}0`;
        }
        return re;
    }

    render() {
        const st = this.state.m;
        return <section className="home" style={{minHeight:this.state.minHeight}}>
            <div>
                <div>
                    <dl className='rightSvg top'>
                    <dd>本期应还款(元)<span>{this.getLocalNum(st.currentShouldRepaymentAmount)}</span></dd>
                    <dd>全部应还款(元)<span>{this.getLocalNum(st.totalShouldRepaymentAmount)}</span></dd>
                    <dd className='todetail' onClick={this.toLoanDetail}>
                        借款记录
                        <span>点击查看</span>
                        <Arrow fill='#fff'/>
                    </dd>
                </dl>

                <div className='main'>
                    <Motion defaultStyle={{x: 0}} style={{x: spring(st.avaiAmount||0)}}>
                          {value => <p>{this.getLocalNum(value.x)}</p>}
                    </Motion>
                    <p className='pulse animated' hidden>
                        {this.getLocalNum(st.avaiAmount)}
                    </p>
                    <span>总额度{this.getLocalNum(st.quotaAmount)}</span>
                    <a href="javascript:;" onClick={this.toLoan.bind(this)}>申请借款</a>
                </div>
                <dl className='operate'>
                    <dd onClick={this.toRaise}>
                        <i className='up animated rotateIn'></i><span>我要提额</span>
                    </dd>
                    <dd onClick={this.toRepayMent}>
                        <i className='re animated rotateIn'></i><span>我要还款</span>
                    </dd>
                </dl>
            </div>
        </div>
        <Loading type={st.loading}/>
        </section>
   }
};

class UnLogin extends Component{
    constructor(props) {
        super(props);
        this.state ={
            minHeight:0,
        }
    }

    componentDidMount() {
        this.setState({
            minHeight:$(window).height() - $('header').height(),
        });
    }

    getLocalNum(num){
        const [i,re,len] = [`${num}`.indexOf('.'),num.toLocaleString(),`${num}`.length];
        if(i === -1){
            return `${re}.00`;
        }else if(len - i === 2){
            return `${re}0`;
        }
        return re;
    }

    toLogin(){
        location.hash = '#/login';
    }

    render() {
        return <section onClick={this.toLogin} className="home unLogin" style={{minHeight:this.state.minHeight}}>
            <div>
                <div>
                    <dl className='rightSvg top'>
                    <dd>本期应还款(元)<span>0.00</span></dd>
                    <dd>全部应还款(元)<span>0.00</span></dd>
                    <dd className='todetail'>
                        借款记录
                        <span>点击查看</span>
                        <Arrow fill='#fff'/>
                    </dd>
                </dl>
                <div className='main'>
                    <p className='pulse animated'>{this.getLocalNum(8000)}</p>
                    <span>总额度{this.getLocalNum(20000)}</span>
                </div>
                <dl className='operate'>
                    <dd>
                        <i className='up animated rotateIn'></i>
                        <span>个人借款</span>
                        <label>（暂未开通）</label>
                    </dd>
                    <dd>
                        <i className='re animated rotateIn'></i><span>商户借款</span>
                        <label>（仅限杭州地区）</label>
                    </dd>
                </dl>
            </div>
        </div>
        </section>
   }
};


export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state ={
            height:$(window).height(),
        }
    }

    componentDidMount(){
        __event__.setHeader({
            title: '米庄贷',
            backBtn:false,
            rightBtn:['','#/personal','personal']
        });
    }

    componentWillUnmount() {
        __event__.setHeader({
            title: '米庄贷',
            backBtn:false,
            rightBtn:false
        });
    }

    getLocalNum(num){
        const [i,re,len] = [`${num}`.indexOf('.'),num.toLocaleString(),`${num}`.length];
        if(i === -1){
            return `${re}.00`;
        }else if(len - i === 2){
            return `${re}0`;
        }
        return re;
    }


    render() {
        if(Cookies.getCookie('userKey')){
            return <Loged/>
        }else{
            return <UnLogin/>
        }
   }
};

ReactMixin.onClass(Loged, Reflux.connect(store,'m'));