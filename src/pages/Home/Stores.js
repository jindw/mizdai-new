import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    home(){
        this.data.loading = '';
        DB.Home.home().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    applystatus(){
        DB.Personal.applystatus().then(data=>{
            if(data.process === 'PASS'){
                this.data.base = true;
                this.trigger(this.data);
            }
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            loading:'on',
            base:false,
        };
        return this.data;
    }
});