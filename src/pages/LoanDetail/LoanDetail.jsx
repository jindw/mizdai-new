import React,{Component} from  'react'
import Main from './Main.jsx'
import Plan from './Plan.jsx'
import List from './List.jsx'

export default class LoanDetail extends Component {

	constructor(props) {
        super(props);
    }

	render() {
		const params = this.props.match.params;
		if(params.loanid){
			if(params.plan){
				return <Plan/>
			}
			return <Main {...this.props}/>
		}
		return <List/>
    }
};
