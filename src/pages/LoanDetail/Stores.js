import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getLoanList(){
        this.data.loading = '';
    	DB.Loan.getLoanList().then(data=>{
            _assign(this.data,data);
            if(!data.loans.length){
                this.data.nulls = true;
            }
            this.trigger(this.data);
    	},data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    getLoanDetail(loanNumber){
        this.data.loading = '';
        DB.Loan.getLoanDetail({
            loanNumber
        }).then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        }, data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            break:false,
            loans:[],
            loading:'on',
            loan:{
                simpleLoanStatus:'',
            },
            nulls:'',
        };
        return this.data;
    }
});