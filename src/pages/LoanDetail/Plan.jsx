import React,{Component} from  'react'
import './Plan.scss'

export default class Plan extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        __event__.setHeader({
            title: '还款计划',
            backBtn:true
        });
    }

    render() {
    	return <section className='loanBkPlan'>
    		<dl>
    			<dd className='list_plan'>
    				<label>还款日</label>
    				<span>8月20日</span>
    				<div>825.25</div>
    				<em>含利息25.25元</em>
    			</dd>
    			<dd className='list_plan'>
    				<label>还款日</label>
    				<span>8月20日</span>
    				<div>825.25</div>
    				<em>含利息25.25元</em>
    			</dd>
    			<dd className='list_plan'>
    				<label>还款日</label>
    				<span>8月20日</span>
    				<div>825.25</div>
    				<em>含利息25.25元</em>
    			</dd>
    		</dl>
    	</section>
   }
}