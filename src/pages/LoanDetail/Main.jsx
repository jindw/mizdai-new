import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Cookies from '../../components/Cookie'
import Modal from 'react-modal'
import Loading from '../../components/Loading'
import ReturnPlan from '../../components/ReturnPlan'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './Main.scss'

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state={
            showAllFees:false,
            planState:false,
        }
        Actions.getLoanDetail(this.props.match.params.loanid);
    }

    componentDidMount(){
        __event__.setHeader({
            title: '借款详情',
            backBtn:true,
        });
    }

    showAllFee(){
		this.setState({showAllFees:!this.state.showAllFees});
	}

    showPlan(){
        this.setState({
            planState: true,
        });
    }

	getReturnPlan(t){
        const loan = this.state.m.loan;
		if(_includes(loan.simpleLoanStatus,'打款')&&!_includes(loan.simpleLoanStatus,'打款中')){
            if(loan.payOff){
                return <dl>
                    <dd>还款状态
                        <span className='paid'>已还清</span>
                    </dd>
                </dl>
            }
			return <dl>
                    <dd onClick={this.showPlan.bind(this)} className='rightSvg'>还款计划
                        <Arrow fill='#3366cc'/>
                    </dd>
            		<dd style={{display:(loan.divideSettlementDate?'':'none')}}>下一还款日
                        <span>{new Date((loan.divideSettlementDate)).toLocaleDateString()}</span>
                    </dd>
            		<dd style={{display:(loan.divideRepayAmount?'':'none')}}>下一期还款金额
                        <span>{(loan.divideRepayAmount||0).toFixed(2)}元</span>
                    </dd>
            	</dl>
            }else{
                const [simpleLoanStatus,dt] = [loan.simpleLoanStatus,loan.createDate];
            	return <dl>
            		<dd style={{display:(simpleLoanStatus?'':'none')}}>申请状态
                        <span className='paid'>{simpleLoanStatus}</span>
                    </dd>
            		<dd style={{display:(dt?'':'none')}}>申请日期
                        <span>{(new Date(dt)).toLocaleDateString()}</span>
                    </dd>
            	</dl>
            }
	}

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
    }

    render() {
        const st = this.state.m;
    	return <section className='personal loandetail'>
                <div className='me rightSvg'>
                	<div className='top'>
                		待还款（元）
                		<p className='number pulse animated'>{(st.loan.remainRepayAmount||0).toFixed(2)}</p>
                		<ul>
                			<li>
                				<label>本金（元）</label>
                				<span className='number'>{(st.loan.remainRepayPrincipal||0).toFixed(2)}</span>
                			</li>
                			<li>
                				<label>{`${st.loan.termType === '3'?'日':'月'}利率`}</label>
                				<span className='number'>{(st.loan.loanRatio*100||0).toFixed(`${st.loan.termType === '3'?3:2}`)}%</span>
                			</li>
                		</ul>
                	</div>
                	<dl>
                		<dd>借款期限<span>{st.loan.loanCycleName}</span></dd>
                		<dd onClick={this.showAllFee.bind(this)} className={this.state.showAllFees?'turnDown':''}>综合费用(元)
                			<span className='number showall'>{(st.loan.comprehensiveFee||0).toFixed(2)}</span>
                			<Arrow fill='#3366cc'/>
                		</dd>
                	</dl>
                	<div className={this.state.showAllFees?'fee show':'fee'}>
                		<div>利息{(st.loan.expectIncome||0).toFixed(2)}元</div>
                		<div>手续费{(st.loan.fee||0).toFixed(2)}元</div>
                	</div>
                	{this.getReturnPlan()}
                    <Modal
                        isOpen={st.modalIsOpen}
                        onAfterOpen={this.openModal.bind(this)}
                        onRequestClose={this.closeModal.bind(this)}
                        closeTimeoutMS={150}
                    >
                        <div className='content'>
                            <i onClick={this.closeModal.bind(this)}>&times;</i>
                            {st.errorMsg}
                        </div>
                        <div className='btn' onClick={this.closeModal.bind(this)}>
                            <a href="javascript:;">我知道了</a>
                        </div>
                    </Modal>
                </div>
                <ReturnPlan times={this.state.times}
                    loanAmount = {st.loan.amount}
                    loanCycle={st.loan.loanCycle}
                    rate={st.loan.loanRatio}
                    termType={st.loan.termType}
                    planState={this.state.planState}
                    closePlan={()=>{
                        this.setState({
                            planState: false
                        });
                    }}
                />
                <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Main, Reflux.connect(store,'m'));
