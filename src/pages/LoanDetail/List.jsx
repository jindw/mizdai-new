import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

import './List.scss'

export default class List extends Component {

	constructor(props) {
        super(props);
        Actions.getLoanList();
    }

    toDetail(id){
    	location.hash = `#/loanDetail/${id}`;
    }

    componentDidMount(){
        __event__.setHeader({
            title: '借款列表',
            backBtn:true
        });
    }

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            location.hash = '/#';
        }
        Actions.closeModal();
    }

	render() {
		const [st,loans] = [this.state.m,[]];
		st.loans.forEach((itm,ind)=>{
			const dt = new Date(itm.createDate);
			loans.push(<dd key={`loans_${ind}`} 
                    onClick={this.toDetail.bind(this,itm.loanNumber)}>
					<span>{(itm.amount).toFixed(2)}</span>
					<label>{`${dt.getFullYear()}年${dt.getMonth()+1}月${dt.getDate()}日`}申请借款（{itm.loanCycleName}）</label>
					<em style={{display:(itm.payOff?'none':'')}} className={itm.loanStatus}>{itm.simpleLoanStatus}</em>
                    <em style={{display:(itm.payOff?'':'none')}} className='clean'>已还清</em>
					<Arrow fill='#3366cc'/>
				</dd>);
		});
		return <section className='loan_list'>
			<dl id={st.nulls?'list_null':''} className='rightSvg'>
				{loans}
			</dl>
			<Modal
                isOpen={st.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
              <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                {st.errorMsg}
              </div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <Loading type={st.loading}/>
		</section>
    }
};

ReactMixin.onClass(List, Reflux.connect(store,'m'));