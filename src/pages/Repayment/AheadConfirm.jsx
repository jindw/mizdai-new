import React,{Component} from  'react'
import './AheadConfirm.scss'
export default class AheadConfirm extends Component {

	constructor(props) {
        super(props);
    }

    componentDidMount(){
        __event__.setHeader({
            title: '还款',
            backBtn:true
        });
    }

	render() {
		return <section className='repayment_ahead_confirm'>
			<div>还款本金(元)<span>4250.00</span></div>
            <p>选择多笔借款时需全部结清</p>
            <div>利息<span>16.00</span></div>
            <div>手续费<span>0.00</span></div>
            <div>还款总额<span>4266.00</span></div>
            <a className='btn_abs_bottom' href="javascript:;">确定</a>
		</section>
    }
};
