import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    outstanding(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.RepayMent.outstanding().then(data=>{
            _assign(this.data,data);
            if(!data.loanBills.length){
                this.data.nulls = true;
            }
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    outstandingloan(){
        this.data.loading = '';
        DB.RepayMent.outstandingloan().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    getRecord(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.RepayMent.getRecord().then(data=>{
            _assign(this.data,data);
            if(!data.repayments.length){
                this.data.nulls = true;
            }
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            loading:'on',
            break:false,
            repayments:[],
            loanBills:[],
            nulls:'',
        };
        return this.data;
    }
});