import React,{Component} from  'react'
import All from './All.jsx'
import This from './This.jsx'
import Record from './Record.jsx'
import Ahead from './Ahead.jsx'
import AheadConfirm from './AheadConfirm.jsx'

export default class RepayMent extends Component {

	constructor(props) {
        super(props);
    }

	render() {
		const params = this.props.match.params;
		if(params.id){
			return <AheadConfirm/>
		}
		switch(params.type){
			case 'this':
				return <This/>
			case 'record':
				return <Record/>
			case 'ahead':
				return <Ahead/>
			default:
				return <All/>
		}

    }
};
