import Reflux,{createActions} from 'reflux'

export default createActions([
	'getRecord',
	'closeModal',
	'outstandingloan',
	'outstanding',
	]);