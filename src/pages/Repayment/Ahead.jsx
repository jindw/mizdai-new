import React,{Component} from  'react'
import './Ahead.scss'

export default class Ahead extends Component {

	constructor(props) {
        super(props);
        this.state = {
        	aheadMoney:0
        }
    }

    componentDidMount(){
        __event__.setHeader({
            title: '提前还款',
            backBtn:true
        });
        this.Selects(1);
    }

    Selects(r){
    	$('.on').removeClass('on');
    	this.refs[r].className='on';
    	this.setState({
    		aheadMoney:$(this.refs[`m_${r}`]).text()
    	})
    }

    onSubmit(){
    	location.hash = '#/repayment/ahead/1'
    }

	render() {
		return <section className='repayment_ahead'>
			<dl>
				<dt>共2笔借款未还清，提前还款无手续费</dt>
				<dd ref='1' onClick={this.Selects.bind(this,'1')} className='on'>
					<p>2016/08/20  借款2000.00元</p>
					<div ref='m_1' className='number'>2000.00</div>
					<i></i>
				</dd>
			
				<dd ref='2' onClick={this.Selects.bind(this,'2')}>
					<p>2016/08/20  借款5000.00元</p>
					<div ref='m_2' className='number'>5000.00</div>
					<i></i>
				</dd>
			</dl>
			<footer>
				<div>共<em>{this.state.aheadMoney}</em>元</div>
				<a href="javascript:;" onClick={this.onSubmit.bind(this)}>提前还款</a>
			</footer>
		</section>
    }
};
