import React,{Component} from  'react'
import Loading from '../../components/Loading'
import Modal from 'react-modal'

import Reflux from 'reflux'
import ReactMixin from 'react-mixin'
import Actions from './Actions'
import store from './Stores'
import './Record.scss'

export default class Record extends Component {

	constructor(props) {
        super(props);
        Actions.getRecord();
        this.state = {
            minHeight:0,
        }
    }

    componentDidMount(){
        __event__.setHeader({
            title: '还款记录',
            backBtn:true
        });
        this.setState({
            minHeight: $(window).height() - $('header').height(),
        });
    }

    openModal(){}

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
    }

	render() {
        const [st,repayments] = [this.state.m,[]];
        let repaymentsYear;
        st.repayments.forEach((itm,ind)=>{
            const dt = new Date(itm.settlementDate);
            if(repaymentsYear !== dt.getFullYear()){
                repayments.push(<dt key={`repayments_year_${ind}`}>{dt.getFullYear()}年</dt>);
                repaymentsYear = dt.getFullYear();
            }
            repayments.push(<dd key={`repayments_${ind}`} className='list_plan'>
                    <label>还款日(自动还款)</label>
                    <span>{dt.getMonth()+1}月{dt.getDate()}日</span>
                    <div>{(itm.totAmount).toFixed(2)}</div>
                    <em>{itm.bankCard?itm.bankCard.bankName:''}(尾号：{itm.bankCard?itm.bankCard.tail:''})</em>
                </dd>);
        })
		return <section style={{minHeight:this.state.minHeight}} className='repayment_record'>
			<dl id={st.nulls?'list_null':''} >
                {repayments}
			</dl>
            <Modal
                isOpen={st.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
              <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                {st.errorMsg}
              </div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <Loading type={st.loading}/>
		</section>
    }
};

ReactMixin.onClass(Record, Reflux.connect(store,'m'));