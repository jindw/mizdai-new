import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Loading from '../../components/Loading'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'
import './All.scss'
export default class All extends Component {

	constructor(props) {
        super(props);
        Actions.outstandingloan();
    }

    componentDidMount(){
        __event__.setHeader({
            title: '还款',
            backBtn:true,
            rightBtn:false
        });
    }

    toThis(){
    	location.hash = '#/repayment/this';
    }

    toAhead(){
    	location.hash = '#/repayment/ahead';
    }

	render() {
		const st = this.state.m;
		return <section className='repayment_all'>
			<div className='detail'>
				<p className='number pulse animated'>{st.outstandingLoanPrincipal||0}</p>
				<span>共<em>{st.outstandingLoanCount}</em>笔借款未结清</span>
			</div>
			<dl className='rightSvg list_return'>
				<dt onClick={this.toThis}>
					<span>按期还款</span>
					<label hidden>本期<em>1200.25元</em></label>
					<Arrow fill='#3366cc'/>
				</dt>
				<dd onClick={this.toAhead} hidden>
					<span>提前还款</span>
					<label>共两笔</label>
					<Arrow fill='#3366cc'/>
				</dd>
			</dl>
			<Loading type={st.loading}/>
		</section>
    }
};

ReactMixin.onClass(All, Reflux.connect(store,'m'));