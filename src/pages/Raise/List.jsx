import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Loading from '../../components/Loading'
import Modal from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from './Actions'
import store from './Stores'

let [Swiper_Title,Swiper_Container] = [,];
export default class Main extends Component {
	constructor(props) {
        super(props);
        Actions.getUserauths(); 
        Actions.applystatus();
        Actions.getUserextauths();
        this.state = {
            minHeight:0,
        }
    }

    componentDidMount() {
        __event__.setHeader({
            title: '我要提额',
            backBtn:'#/'
        });

        this.setState({
            minHeight: $(window).height() - $('header').height(),
        });

        Swiper_Title = new Swiper('#swiper-title',{
            resistanceRatio : 0,
		})
        Swiper_Container = new Swiper('#swiper-container',{
            effect : 'coverflow',
            coverflow: {
                rotate: 30,
                stretch: 10,
                depth: 60,
                modifier: 2,
                slideShadows : true
            },
            iOSEdgeSwipeDetection : true,
            resistanceRatio : 0,
            observer:true,
            initialSlide:localStorage.raiseRight === 'true'?1:0,
            onReachEnd:()=>$(this.refs.titlePro).addClass('on').prev().removeClass('on'),
            onReachBeginning:()=>$(this.refs.titleBase).addClass('on').next().removeClass('on')
        })  
    }

    componentWillUnmount() {
        localStorage.raiseRight = false;
    }

    toLeft(){
        Swiper_Container.slidePrev();
    }

    toRight(){
        Swiper_Container.slideNext();
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    toRaise(it,type,up){
        if(up === 0&&!type){
            this.setState({
                modalIsOpen:true,
                errmsg:'请先完成以上认证',
            })
        }else if(!type){
            location.hash = `#/raise/${it}`;
        }
    }

    render() {
        const st = this.state.m;
    	return <section className='raise rightSvg' style={{minHeight:this.state.minHeight}}>
    			<div id="swiper-title">
				  <div className="swiper-wrapper">
				    <div ref='titleBase' onClick={this.toLeft}
                    className="swiper-slide swiper-no-swiping on">基础授信</div>
				    <div ref='titlePro' onClick={this.toRight}
                    className="swiper-slide swiper-no-swiping">信用提额</div>
				  </div>
				</div>
                <div id="swiper-container">
				  <div className="swiper-wrapper">
				    <div className="swiper-slide">
                        <dl>
                            <dd onClick={this.toRaise.bind(this,'certification',st.REALAUTH)}>实名认证
                                <label className={st.REALAUTH?'on':''}>
                                    {`${st.REALAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.REALAUTH?'#ddd':'#3366cc'}/>
                            </dd>
                            <dd onClick={this.toRaise.bind(this,'bank',st.BINDCARDAUTH,st.REALAUTH)}>添加银行卡
                                <label className={st.BINDCARDAUTH?'on':''}>
                                    {`${st.BINDCARDAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.BINDCARDAUTH?'#ddd':'#3366cc'}/></dd>
                            <dd onClick={this.toRaise.bind(this,'contact',st.CONTACTSAUTH,st.BINDCARDAUTH)}>添加联系人
                                <label className={st.CONTACTSAUTH?'on':''}>
                                    {`${st.CONTACTSAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.CONTACTSAUTH?'#ddd':'#3366cc'}/>
                            </dd>
                            <dd className='shanghu' onClick={this.toRaise.bind(this,'commercial',st.MERCHANTAUTH,st.CONTACTSAUTH)}>
                                <i></i>
                                <span>商户资质证明</span>
                                <label className={st.MERCHANTAUTH?'on':''}>
                                    {`${st.MERCHANTAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.MERCHANTAUTH?'#ddd':'#3366cc'}/>
                            </dd>
                            <dd className='pact' onClick={this.toRaise.bind(this,'pact',st.MERCHANTRENTAUTH,+st.MERCHANTAUTH*+st.CONTACTSAUTH)}>
                                <i></i>
                                <span>经营性房屋合同</span>
                                <label className={st.MERCHANTRENTAUTH?'on':''}>
                                    {`${st.MERCHANTRENTAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.MERCHANTRENTAUTH?'#ddd':'#3366cc'}/>
                            </dd>
                        </dl>
                        <p>{st.applystatus}</p>
                        <a style={{display:(st.coodLoan?'':'none')}} className='btn_abs_bottom' href="#/loan">申请借款</a>
                    </div>
				    <div className="swiper-slide">
                        <dl className='prove'>
                            <dd className='credit' onClick={this.toRaise.bind(this,'credit',st.PBCAUTH)}>
                                人行征信
                                <label className={st.PBCAUTH?'on':''}>
                                    {`${st.PBCAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.PBCAUTH?'#ddd':'#3366cc'}/>
                            </dd>
                            <dd className='marry' onClick={this.toRaise.bind(this,'marry',st.MARITALAUTH)}>
                                婚姻状况
                                <label className={st.MARITALAUTH?'on':''}>
                                    {`${st.MARITALAUTH?'已':'未'}提交`}
                                </label>
                                <Arrow fill={st.MARITALAUTH?'#ddd':'#3366cc'}/>
                            </dd>

                            <dd className='live' onClick={this.toRaise.bind(this,'live',st.LIVEAUTH)}>
                                居住地信息
                                <label className={st.LIVEAUTH?'on':''}>
                                    {`${st.LIVEAUTH?'已':'未'}提交`}</label>
                                <Arrow fill={st.LIVEAUTH?'#ddd':'#3366cc'}/>
                            </dd>

                            <dd className='census' onClick={this.toRaise.bind(this,'census',st.CENSUSAUTH)}>
                                <i></i>户籍证明
                                <label className={st.CENSUSAUTH?'on':''}>
                                    {`${st.CENSUSAUTH?'已':'未'}提交`}</label>
                                <Arrow fill={st.CENSUSAUTH?'#ddd':'#3366cc'}/>
                            </dd>

                            <dd style={{display:'none'}} className='edu' onClick={this.toRaise.bind(this,'edu',st.EDUCATIONAUTH)}>
                                个人学历
                                <label className={st.EDUCATIONAUTH?'on':''}>
                                    {`${st.EDUCATIONAUTH?'已':'未'}提交`}</label>
                                <Arrow fill={st.EDUCATIONAUTH?'#ddd':'#3366cc'}/>
                            </dd>
                            <dd style={{display:'none'}} className='zhima' onClick={this.toRaise.bind(this,'zhima',0)}>
                                <i></i>芝麻信用
                                <label className=''>未提交</label>
                                <Arrow fill='#3366cc'/>
                            </dd>
                            <dd style={{display:'none'}} onClick={this.toRaise.bind(this,'car')}>车房信息<label className='on'>已认证</label><Arrow fill='#3366cc'/></dd>
                            <dd style={{display:'none'}} onClick={this.toRaise.bind(this,'payment')}>缴费信息<label className='on'>已认证</label><Arrow fill='#3366cc'/></dd>
                            <dd style={{display:'none'}} onClick={this.toRaise.bind(this,'taobao')}>网购信息<label className='on'>已认证</label><Arrow fill='#3366cc'/></dd>
                            <dd style={{display:'none'}} onClick={this.toRaise.bind(this,'houseingfund')}>公积金<label className='on'>已认证</label><Arrow fill='#3366cc'/></dd>
                            <dd style={{display:'none'}} onClick={this.toRaise.bind(this,'work')}>工作信息<label className='on'>已认证</label><Arrow fill='#3366cc'/></dd>
                            <dd style={{display:'none'}} onClick={this.toRaise.bind(this,'bankwater')}>网银流水<label className='on'>已认证</label><Arrow fill='#3366cc'/></dd>
                        </dl>
                    </div>
				  </div>
				</div>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'>
                    <i onClick={this.closeModal.bind(this)}>&times;</i>
                     {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Main, Reflux.connect(store,'m'));