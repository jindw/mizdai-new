import React,{Component} from  'react'
import List from './List.jsx'
import Certification from './Jsx/Certification.jsx'
import Marry from './Jsx/Marry.jsx'
import Payment from './Jsx/Payment.jsx'
import Work from './Jsx/Work.jsx'
import Contact from './Jsx/Contact.jsx'
import Taobao from './Jsx/Taobao.jsx'
import Credit from './Jsx/Credit.jsx'
import Bank from './Jsx/Bank.jsx'
import Edu from './Jsx/Edu.jsx'
import Commercial from './Jsx/Commercial.jsx'
import Live from './Jsx/Live.jsx'
import Car from './Jsx/Car.jsx'
import BankWater from './Jsx/BankWater.jsx'
import HouseingFund from './Jsx/HouseingFund.jsx'
import Zhima from './Jsx/Zhima.jsx'
import Pact from './Jsx/Pact.jsx'
import Census from './Jsx/Census.jsx'

import Actions from './Actions'

import './Raise.scss'
export default class Raise extends Component {
    constructor(props) {
        super(props);
        Actions.getUserMsg();
    }

    render() {
    	switch(this.props.match.params.type){
    		case 'certification':
    			return <Certification/>
    		case 'marry':
    			return <Marry/>
        case 'payment':
            return <Payment {...this.props}/>
        case 'work':
            return <Work/>
        case 'contact':
            return <Contact/>
        case 'taobao':
            return <Taobao/>
        case 'credit':
            return <Credit/>
        case 'bank':
            return <Bank/>
        case 'edu':
            return <Edu/>
        case 'commercial':
            return <Commercial/>
        case 'live':
            return <Live/>
        case 'car':
            return <Car/>
        case 'bankwater':
            return <BankWater/>
        case 'houseingfund':
            return <HouseingFund {...this.props}/>
        case 'zhima':
            return <Zhima/>
        case 'pact':
            return <Pact/>
        case 'census':
            return <Census/>
    		default:
    			return <List/>
    	}
   }
}
