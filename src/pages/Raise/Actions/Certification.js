import Reflux,{createActions} from 'reflux'

export default createActions([
	'getRealNameAuth',
	'closeModal',
	'getSignature',
	'imgRemove',
	'getUserauths',
	]);