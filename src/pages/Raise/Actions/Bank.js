import Reflux,{createActions} from 'reflux'

export default createActions(
	['getBankList',
	'closeModal',
	'sendBindvalidateCode',
	'confirm',
	'getUserauths',
	]);