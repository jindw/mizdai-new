import Reflux,{createActions} from 'reflux'

export default createActions([
	'getProvince',
	'getCity',
	'getCounty',
	'companies',
	'getSignature',
	'closeModal',
	'getUserMsg',
	'getUserauths',
	]);