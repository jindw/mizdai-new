import Reflux,{createActions} from 'reflux'

export default createActions([
	'getSignature',
	'census',
	'closeModal',
	'getUserextauths',
	]);