import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserMsg(){
        DB.Personal.getUserMsg();
    },

    applystatus(){
        DB.Personal.applystatus().then(data=>{
            let applystatus;
            switch(data.process){
                case 'APPLYING':
                    applystatus = '您的认证信息已提交，业务员会在2个工作日内上门服务。';
                    break;
                case 'REJECT':
                    applystatus = '很抱歉，您的认证信息未审核通过，谢谢您的配合。';
                    break;
                case 'PASS':
                    applystatus = '您的认证信息已审核通过，可申请借款。';
                    this.data.coodLoan = true;
            }
            this.data.applystatus = applystatus;
            this.trigger(this.data);
        }, data=>{
            this.data.applystatus = '基础信息提交后，会在48小时内审核完成。';
            this.trigger(this.data);
        });
    },

    getUserextauths(){
        this.data.loading = '';
        DB.Personal.getUserextauths().then(data=>{
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            this.trigger(this.data);
        }, data=>{
            assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    getUserauths(){
        this.data.loading = '';
        DB.Personal.getUserauths().then(data=>{
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            this.trigger(this.data);
        }, data=>{
            assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            loading:'on',
            myPass:'',
            coodLoan:false,
        };
        return this.data;
    }
});