import React,{Component} from  'react'
import Modal from 'react-modal'
import ModalImg from 'react-modal'
import _times from 'lodash/times'
import Loading from '../../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Pact'
import store from '../Stores/Pact'

import '../Scss/Pact.scss'

export default class Pact extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            report:null,
            modalImgIsOpen:false,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        Actions.getUserauths();
        localStorage.raiseRight = false;
    }
 
	componentDidMount() {
	    __event__.setHeader({
            title: '经营性房屋合同',
            backBtn:true,
        });
	}


	openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        })
    }

    openModalImgBefore(img){
        this.setState({
            modalImgIsOpen:true,
            imgShow:img,
        })
    }

    openModalImg(){}

    closeModalImg() {
        this.setState({modalImgIsOpen:false})
    }


    toSubmit(){
        const photo = this.state.m.myReport;
        if(!photo.length){
            this.setState({
                modalIsOpen:true,
                errmsg:'请上传您的经营性房屋合同',
            });
        }else{
            Actions.housecontfaractpics();
        }
    }

    myReport(){
        const file = this.refs.file;
        if(file.files.length){
            Actions.getSignature(file);
        }
    }

    imgRemove(imgIndex){
        Actions.removeImg(imgIndex);
    }

    render() {
    	const st = this.state.m;
        const len = st.myReport.length;
        let pacts = [];
        _times(Math.min(Math.max(3,len+1),9), ind => {
            const rp = st.myReport[ind]||'';
            const props = {
                key:`pacts_${ind}`,
                ref:`pacts_${ind}`,
                className:len > ind ? 'wait on' : 'wait' ,
                style:{
                    backgroundImage:`url(${rp})`
                },
            }
            pacts.push(<dd {...props}><a onClick={this.openModalImgBefore.bind(this,rp)}/><i onClick={this.imgRemove.bind(this,ind)}>&times;</i>+</dd>);
        });
       	return <section className='safety pact'>
                <dl>
                    <dt>请上传您的经营性房屋合同</dt>
                    {pacts}
                    <input style={{display:(len<9?'':'none')}} 
                    onChange={this.myReport.bind(this)} 
                    type="file" ref='file' accept="image/*"/>
                    <p style={{display:'none'}}>更多...</p>
                </dl>
                <p>注：经营性房屋合同即您用于店铺经营的房屋租赁或买卖合同</p>
                <a className={`btn_abs_bottom android_${this.state.anDroid}`} 
                 href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>

                <ModalImg
                    isOpen={this.state.modalImgIsOpen}
                    onAfterOpen={this.openModalImg.bind(this)}
                    onRequestClose={this.closeModalImg.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            background: 'none',
                            border:0,
                            textAlign:'center',
                            display:'-webkit-flex',
                        }
                    }}
                >   
                    <a className='close' onClick={this.closeModalImg.bind(this)} href="javascript:;"></a>
                    <img src={this.state.imgShow}/>
                </ModalImg>

                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                    {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Pact, Reflux.connect(store,'m'));