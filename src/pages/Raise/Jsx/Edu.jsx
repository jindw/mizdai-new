import React,{Component} from 'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import ModalSelectEdu from 'react-modal'
import ModalSelectSchool from 'react-modal'
import ModalSelectPlace from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Edu'
import store from '../Stores/Edu'

import '../Scss/Edu.scss'

export default class Edu extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            place:'',
            school:'',
            edu:'',
            modalSelectEduIsOpen:false,
            modalSelectSchoolIsOpen:false,
            modalPlaceIsOpen:false,
            swiper:null,
            cityName:'',
            title:'学历信息',
        }
        Actions.getProvince();
    }

	componentDidMount() {
	     __event__.setHeader({
            title: this.state.title,
            backBtn:true,
            raiseRight:true
        });
	}

	openModal(){
        this.setState({modalIsOpen:true})
    }

    closeModal() {
        this.setState({modalIsOpen:false})
    }

    openModalSelectEdu(){
        this.setState({modalSelectEduIsOpen:true})
    }

    closeModalSelectEdu(){
        this.setState({modalSelectEduIsOpen:false})
    }

    selectEdu(edu,schoolCode){
        this.setState({
            modalSelectEduIsOpen:false,
            edu:edu,
            schoolCode:schoolCode,
        })
    }

    openModalSelectSchool(){
        if(this.state.cityName){
            this.setState({modalSelectSchoolIsOpen:true});
        }else{
            this.setState({
                modalIsOpen: true,
                errmsg:'请选择您的学校所在地区',
            });
        }
    }

    closeModalSelectSchool(){
        this.setState({modalSelectSchoolIsOpen:false})
    }

    selectSchool(schoolName){
        this.setState({
            modalSelectSchoolIsOpen:false,
            school:schoolName,
        })
    }

    openModalPlace(){
        this.setState({modalPlaceIsOpen:true});
        const sw =  new Swiper('.swiper-container',{
            swipeHandler : 'none',
            effect : 'coverflow',
            slidesPerView: 1,
            centeredSlides: true,
            coverflow: {
                rotate: 30,
                stretch: 10,
                depth: 60,
                modifier: 2,
                slideShadows : true
            },
            speed:200,
        });

        this.setState({swiper:sw});

        __event__.setHeader({
            title: this.state.title,
            backBtn:false,
            raiseRight:true
        });
    }

    closeModalPlace(){
        this.setState({modalPlaceIsOpen:false});
        this.componentDidMount();
    }

    SelectCity(province){
        Actions.getCity(province);
        this.state.swiper.slideNext();
    }

    prevModalPlace(){
        this.state.swiper.slidePrev();
    }

    selectPlace(city){
        this.setState({
            cityName:city.name,
            cityCode:city.code,
        });
        this.closeModalPlace();
    }

    toSubmit(){
        const [cityName,school,edu] = [this.state.cityName,this.state.school,this.state.edu];
        let err;
        if(!cityName){
            err = '请选择您的学校所在地区';
        }else if(!school){
            err = '请选择您的学校';
        }else if(!edu){
            err = '请选择您的学历';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err
            });
        }else{
            Actions.education({
                cityCode:this.state.cityCode,
                schoolCode:this.state.schoolCode,
            });
        }
    }

    render() {
    	const st = this.state.m;

        let [provinces,cities] = [[],[]];
        st.provinces.forEach((itm,ind)=>provinces.push(<dd key={`province_${ind}`} onClick={this.SelectCity.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));
        
        st.cities.forEach((itm,ind)=>cities.push(<dd key={`city_${ind}`} onClick={this.selectPlace.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

       	return <section className='safety edu'>
                <dl className='operate rightSvg'>
                	<dd onClick={this.openModalPlace.bind(this)}>
                		<span>选择地区</span>
	                	<label className={this.state.cityName?'':'off'}>{this.state.cityName?`${st.sProvince} ${this.state.cityName}`:'请选择您的学校所在地区'}</label>
	                	<Arrow fill='#3366cc'/>
                	</dd>
                	<dd onClick={this.openModalSelectSchool.bind(this)}>
                        <span>选择学校</span>
	                	<label className={this.state.school?'':'off'}>{this.state.school||'请选择您的学校'}</label>
	                	<Arrow fill='#3366cc'/>
	                </dd>
	                <dd onClick={this.openModalSelectEdu.bind(this)}><span>选择学历</span>
	                	<label className={this.state.edu?'':'off'}>{this.state.edu||'请选择您的学历'}</label>
	                	<Arrow fill='#3366cc'/>
	                </dd>
                </dl>
        		
                <a onClick={this.toSubmit.bind(this)} className='btn_abs_bottom' href="javascript:;">提交</a>

                <ModalSelectPlace
                    isOpen={this.state.modalPlaceIsOpen}
                    onAfterOpen={this.openModalPlace.bind(this)}
                    onRequestClose={this.closeModalPlace.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'#eee',
                        }
                    }}
                >   
                    <div className="swiper-container selectCity">
                        <div ref='swiperWrapper' className="swiper-wrapper">
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.closeModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/></dt>
                                    {provinces}
                                </dl>
                            </div>
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.prevModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/>
                                        <span>{st.sProvince}</span>
                                    </dt>
                                    {cities}
                                </dl>
                            </div>
                        </div>
                    </div>
                </ModalSelectPlace>

                <ModalSelectSchool
                    isOpen={this.state.modalSelectSchoolIsOpen}
                    onAfterOpen={this.openModalSelectSchool.bind(this)}
                    onRequestClose={this.closeModalSelectSchool.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content select'>
                        <i onClick={this.closeModalSelectSchool.bind(this)}>&times;</i>请选择学校
                    </div>
                    <dl>
                        <dd onClick={this.selectSchool.bind(this,'A大学')}>A大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'B大学')}>B大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'C大学')}>C大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                        <dd onClick={this.selectSchool.bind(this,'D大学')}>D大学</dd>
                    </dl>
                </ModalSelectSchool>

                <ModalSelectEdu
                    isOpen={this.state.modalSelectEduIsOpen}
                    onAfterOpen={this.openModalSelectEdu.bind(this)}
                    onRequestClose={this.closeModalSelectEdu.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content select'>
                        <i onClick={this.closeModalSelectEdu.bind(this)}>&times;</i>请选择学历
                    </div>
                    <dl>
                        <dd onClick={this.selectEdu.bind(this,'小学','01')}>小学</dd>
                        <dd onClick={this.selectEdu.bind(this,'初中','02')}>初中</dd>
                        <dd onClick={this.selectEdu.bind(this,'高中','03')}>高中</dd>
                        <dd onClick={this.selectEdu.bind(this,'大专','04')}>大专</dd>
                        <dd onClick={this.selectEdu.bind(this,'本科','05')}>本科</dd>
                        <dd onClick={this.selectEdu.bind(this,'硕士','06')}>硕士</dd>
                        <dd onClick={this.selectEdu.bind(this,'博士','07')}>博士</dd>
                    </dl>
                </ModalSelectEdu>

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{this.state.errmsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(Edu, Reflux.connect(store,'m'));