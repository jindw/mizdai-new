import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import ModalSelectPlace from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Car'
import store from '../Stores/Car'

import '../Scss/Car.scss'

export default class Car extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            photo:null,
            modalPlaceIsOpen:false,
            cityname:'',
            title:'车房信息',
            swiper:null,
        }
        Actions.getProvince();
    }

	componentDidMount() {
	    __event__.setHeader({
            title: this.state.title,
            backBtn:true,
            raiseRight:true
        });
	}

	openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen:false
        })
    }

	myphoto(){
		const file = this.refs.file;
		if(file.files.length){
			const reader = new FileReader();
			reader.readAsDataURL(file.files[0]);
			reader.onload = e =>this.setState({photo:e.target.result});
		}else{
			this.setState({photo:null});
		}
	}

    openModalPlace(){
        this.setState({
            modalPlaceIsOpen:true
        });
        const sw =  new Swiper('.swiper-container',{
            swipeHandler : 'none',
            effect : 'coverflow',
            slidesPerView: 1,
            centeredSlides: true,
            coverflow: {
                rotate: 30,
                stretch: 10,
                depth: 60,
                modifier: 2,
                slideShadows : true
            },
            speed:200,
        });

        this.setState({swiper:sw});

        __event__.setHeader({
            title: this.state.title,
            backBtn:false,
        });
    }

    closeModalPlace(){
        this.setState({modalPlaceIsOpen:false});
        this.componentDidMount();
    }

    SelectCity(province){
        Actions.getCity(province);
        this.state.swiper.slideNext();
    }

    prevModalPlace(){
        this.state.swiper.slidePrev();
    }

    selectPlace(city){
        this.setState({cityName:city.name});
        this.closeModalPlace();
    }

    toSubmit(){
        const [place,num,chejia,photo] = [this.state.cityName,this.refs.num.value,this.refs.chejia.value,this.state.photo];
        let err;
        if(!place){
        	err = '请选择城市';
        }else if(!num){
        	err = '请输入车牌号';
        }else if(!chejia){
        	err = '请输入全部车架号';
        }else if(!photo){
            err = '请上传房产证或租房合同';
        }
        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err
            });
        }
    }

    render() {
    	const st = this.state.m;

        let [provinces,cities] = [[],[]];
        st.provinces.forEach((itm,ind)=>provinces.push(<dd key={`province_${ind}`} onClick={this.SelectCity.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));
        
        st.cities.forEach((itm,ind)=>cities.push(<dd key={`city_${ind}`} onClick={this.selectPlace.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

       	return <section className='safety health edu live car'>
                <dl className='operate'>
                	<dd>
	                    <span>车辆信息</span>
	                </dd>
                    <dd className='rightSvg' onClick={this.openModalPlace.bind(this)}>
                        <span>选择城市</span>
                        <label>{this.state.cityName?`${st.sProvince} ${this.state.cityName}`:''}</label>
                    	<Arrow fill='#3366cc'/>
                    </dd>
                    <dd>
                        <span>车牌号</span>
                        <input ref='num' type="text" placeholder='请输入车牌号'/>
                    </dd>
                    <dd>
                        <span>车架号</span>
                        <input ref='chejia' type="text" placeholder='请输入全部车架号'/>
                    </dd>
                </dl>
                <dl className="operate">
	                <dd>
	                    <span>房产信息</span>
	                </dd>
                	<dd id='photo'>
                        <span>房产证或租房合同</span>
	                	<img style={{opacity:(this.state.photo?'':0)}} src={this.state.photo} alt=""/>
	                	<input onChange={this.myphoto.bind(this)} ref='file' id='file' type="file" accept="image/*"/>
	                	<i></i>
	                </dd>
                </dl>
                
                <a onClick={this.toSubmit.bind(this)} className='btn_abs_bottom' href="javascript:;">提交</a>

                <ModalSelectPlace
                    isOpen={this.state.modalPlaceIsOpen}
                    onAfterOpen={this.openModalPlace.bind(this)}
                    onRequestClose={this.closeModalPlace.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'#eee',
                        }
                    }}
                >   
                    <div className="swiper-container selectCity">
                        <div ref='swiperWrapper' className="swiper-wrapper">
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.closeModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/></dt>
                                    {provinces}
                                </dl>
                            </div>
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.prevModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/>
                                        <span>{st.sProvince}</span>
                                    </dt>
                                    {cities}
                                </dl>
                            </div>
                        </div>
                    </div>
                </ModalSelectPlace>

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{this.state.errmsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(Car, Reflux.connect(store,'m'));