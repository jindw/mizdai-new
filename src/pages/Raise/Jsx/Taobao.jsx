import React,{Component} from  'react'
import Modal from 'react-modal'

import '../Scss/Taobao.scss'

export default class Taobao extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            agree:false
        }
    }

	componentDidMount() {
	     __event__.setHeader({
            title: '淘宝认证',
            backBtn:true,
            raiseRight:true
        });
	}

	openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen:false
        })
    }

	toAgree(){
		this.setState({
			agree:!this.state.agree
		});
	}

	toSubmit(){
        const [username,password,agree] = [this.refs.username.value,this.refs.password.value,this.state.agree];
        let err;
        if(!username){
            err = '请输入您的淘宝账号';
        }else if(!password){
            err = '请输入您的淘宝登录密码';
        }else if(!agree){
            err = '请阅读并同意协议';
        }
        err&&this.setState({
            modalIsOpen:true,
            errmsg:err
        });
	}

    render() {
    	const st = this.state;
       	return <section className='safety taobao'>
                <dl className='operate'>
                	<dd>
                		<span>账户</span>
	                	<input ref='username' placeholder='请输入您的淘宝账号' type="text"/>
                	</dd>
                	<dd><span>密码</span>
	                	<input ref='password' placeholder='请输入您的淘宝登录密码' type="text"/>
	                </dd>
                </dl>
        		<p id='box_check'>
        			<i onClick = {this.toAgree.bind(this)} className={this.state.agree?'on':''}></i>
        			我已阅读并同意 《淘宝***协议》</p>
                <a className='btn_abs_bottom' href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>

                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errmsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};