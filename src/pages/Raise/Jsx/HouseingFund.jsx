import React,{Component} from  'react'
import Modal from 'react-modal'
import includes from 'lodash/includes'

import '../Scss/HouseingFund.scss'

class List extends Component {
	constructor(props) {
        super(props);
        this.state = {
            searchVal:'',
            touchId:'',
            start:false,
            movestart:0,
            lih:0
        }
    }

    componentDidMount() {
	     __event__.setHeader({
            title: '公积金',
            backBtn:true,
            raiseRight:true
        });
	    $('.body').css('background', '#fff');
	    scroll(0,0);
	}

	componentWillUnmount() {
        $('.body').css('background', '#f2f2f2');
	}

    onSearch(){
    	this.setState({
    		searchVal:this.refs.search.value.toLocaleLowerCase()
    	});
    }

    cityList(c){
    	let re = false;
    	let it = [];
		c.forEach((itm,ind)=>{
			if(this.state.searchVal&&ind){
				const sv = this.state.searchVal;
				if(includes(itm.val,sv)||includes(itm.name,sv)){
					re = true;
					it.push(<dd onClick={this.citySelected.bind(this,itm.val)} key={`${itm.val}_${ind}`}>{itm.name}公积金</dd>);
				}
			}else if(ind){
				re = true;
				it.push(<dd onClick={this.citySelected.bind(this,itm.val)} key={`${itm.val}_${ind}`}>{itm.name}公积金</dd>);
			}
		});
		return re?it:false;
    }

    citySelected(it){
    	location.hash = `#/raise/houseingfund/${it}`;
    }

    touchZM(to){
    	this.setState({
    		touchId:+to,
    		start:true,
    		lih:$('.zimus li').height()
    	});
    	this.moveTo(+to);
    }

    moveTo(to){
    	const offs = $('dt').eq(+to).offset();
    	if(!offs)return;
    	const tops = offs.top - $('header').height() - $('.top').height();
    	scroll(0,tops);
    }

    moveZM(e){
    	e.preventDefault();
    	let movestart = this.state.movestart;
    	if(this.state.start&&!!e.targetTouches){
    		this.setState({
    			start:false,
    			movestart:e.targetTouches[0].clientY,
    		});
    		movestart = e.targetTouches[0].clientY;
    	}
    	const moveLi = Math.floor((e.targetTouches[0].clientY - movestart)/this.state.lih);
    	this.moveTo(this.state.touchId + moveLi);
    }

	render(){
		let list = [
			[{title:'热门城市'},{val:'hz',name:'杭州市'},
				{val:'nb',name:'宁波市'},
				{val:'wz',name:'温州市'},
				{val:'hz',name:'湖州市'},
				{val:'sx',name:'绍兴市'},
				{val:'zs',name:'舟山市'},
				{val:'tz',name:'台州市'},
				],
			[{title:'A'},{val:'a1',name:'啊'},
				{val:'a2',name:'啊啊'},
				{val:'a3',name:'啊啊啊'},
				{val:'a4',name:'a4'},
				{val:'a5',name:'aa'},
				{val:'a6',name:'aaa'},
				{val:'a7',name:'aaaaa'},
				],
			[{title:'B'},{val:'bj',name:'北京'},
				{val:'bt',name:' 包头'},
				{val:'bd',name:'保定'},
				{val:'bj',name:'宝鸡'},
				{val:'by',name:'白银'},
				{val:'byeb',name:'白云鄂博'},
				{val:'bdh',name:'北戴河'},
				],
			[{title:'C'},{val:'cq',name:'重庆'},
				{val:'cd',name:'成都'},
				{val:'cs',name:'长沙'},
				{val:'ch',name:'长春'},
				{val:'cd',name:'承德'},
				{val:'cz',name:'常州'},
				{val:'cz',name:'池州'},
				],
		]
		let citys = [];

		list.forEach((itm,ind)=>{
			citys.push(
				<dl key={`list_${ind}`}>
					{
						this.cityList(itm)? <dt id={itm[0].title}>{itm[0].title}</dt>:''
					}
					{this.cityList(itm)}
				</dl>
			)
		});

		return <section className='HouseingFund'>
			<div className='top'>
				<input ref='search' onChange={this.onSearch.bind(this)} type="text" placeholder='搜索城市名'/>
			</div>
			{citys}
			<ul className='zimus'>
				<li onTouchStart={this.touchZM.bind(this,'1')} onTouchMove={this.moveZM.bind(this)}>A</li>
				<li onTouchStart={this.touchZM.bind(this,'2')} onTouchMove={this.moveZM.bind(this)}>B</li>
				<li onTouchStart={this.touchZM.bind(this,'3')} onTouchMove={this.moveZM.bind(this)}>C</li>
				<li onTouchStart={this.touchZM.bind(this,'4')} onTouchMove={this.moveZM.bind(this)}>D</li>
				<li onTouchStart={this.touchZM.bind(this,'5')} onTouchMove={this.moveZM.bind(this)}>E</li>
				<li onTouchStart={this.touchZM.bind(this,'6')} onTouchMove={this.moveZM.bind(this)}>F</li>
				<li onTouchStart={this.touchZM.bind(this,'7')} onTouchMove={this.moveZM.bind(this)}>G</li>
				<li onTouchStart={this.touchZM.bind(this,'8')} onTouchMove={this.moveZM.bind(this)}>H</li>
				<li onTouchStart={this.touchZM.bind(this,'9')} onTouchMove={this.moveZM.bind(this)}>I</li>
				<li onTouchStart={this.touchZM.bind(this,'10')} onTouchMove={this.moveZM.bind(this)}>J</li>
				<li onTouchStart={this.touchZM.bind(this,'11')} onTouchMove={this.moveZM.bind(this)}>K</li>
				<li onTouchStart={this.touchZM.bind(this,'12')} onTouchMove={this.moveZM.bind(this)}>L</li>
				<li onTouchStart={this.touchZM.bind(this,'13')} onTouchMove={this.moveZM.bind(this)}>M</li>
				<li onTouchStart={this.touchZM.bind(this,'14')} onTouchMove={this.moveZM.bind(this)}>N</li>
				<li onTouchStart={this.touchZM.bind(this,'15')} onTouchMove={this.moveZM.bind(this)}>O</li>
				<li onTouchStart={this.touchZM.bind(this,'16')} onTouchMove={this.moveZM.bind(this)}>P</li>
				<li onTouchStart={this.touchZM.bind(this,'17')} onTouchMove={this.moveZM.bind(this)}>Q</li>
				<li onTouchStart={this.touchZM.bind(this,'18')} onTouchMove={this.moveZM.bind(this)}>R</li>
				<li onTouchStart={this.touchZM.bind(this,'19')} onTouchMove={this.moveZM.bind(this)}>S</li>
				<li onTouchStart={this.touchZM.bind(this,'20')} onTouchMove={this.moveZM.bind(this)}>T</li>
				<li onTouchStart={this.touchZM.bind(this,'21')} onTouchMove={this.moveZM.bind(this)}>U</li>
				<li onTouchStart={this.touchZM.bind(this,'22')} onTouchMove={this.moveZM.bind(this)}>V</li>
				<li onTouchStart={this.touchZM.bind(this,'23')} onTouchMove={this.moveZM.bind(this)}>W</li>
				<li onTouchStart={this.touchZM.bind(this,'24')} onTouchMove={this.moveZM.bind(this)}>X</li>
				<li onTouchStart={this.touchZM.bind(this,'25')} onTouchMove={this.moveZM.bind(this)}>Y</li>
				<li onTouchStart={this.touchZM.bind(this,'26')} onTouchMove={this.moveZM.bind(this)}>Z</li>
			</ul>
		</section>
	}
};

class Write extends Component{

	constructor(props) {
        super(props);
        this.state = {
            agree:true,
            cardName:'公积金账号',
            modalIsOpen:false,
            errmsg:'',
        }
    }

	toAgree(){
		const agreed = this.state.agree;
		this.setState({
			agree:!agreed
		})
	}

	selectBtn(cardType){
    	$('.select').removeClass('select');
    	this.refs[cardType].className = 'select';
    	if(cardType === 'gjj'){
    		this.setState({
    			cardName:'公积金账号'
    		});
    	}else{
    		this.setState({
    			cardName:'用户账号'
    		});
    	}
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        this.setState({modalIsOpen:false});
    }

    toSubmit(){
    	const [card,password,agree] = [this.refs.card.value,this.refs.password.value,this.state.agree];
    	let err;
    	if(!card){
    		err = `请输入${this.state.cardName}`;
    	}else if(!password){
    		err = '请输入密码';
    	}else if(!agree){
    		err = '请先同意协议';
    	}

    	if(err){
    		this.setState({
    			modalIsOpen:true,
    			errmsg:err
    		});
    	}
    }

	render(){
		const st = this.state;
		return <section id='houseingfund' className='safety bankwater'>
       			<ul>
       				<li ref='gjj' onClick={this.selectBtn.bind(this,'gjj')} className='select'>公积金账户</li>
       				<li ref='yhzh' onClick={this.selectBtn.bind(this,'yhzh')}>用户账户</li>
       			</ul>
            	<dl className='operate'>
                	<dd><span>{this.state.cardName}</span>
	                	<input ref='card' placeholder={`请输入${this.state.cardName}`} type="text"/>
	                </dd>
	                <dd><span>密码</span>
	                	<input ref='password' placeholder='请输入密码' type="password"/>
	                </dd>
                </dl>
        		<p id='box_check'><i onClick = {this.toAgree.bind(this)} className={this.state.agree?'on':''}></i>我已阅读并同意 《***协议》</p>
                <a onClick={this.toSubmit.bind(this)} className='btn_abs_bottom' href="javascript:;">提交</a>
                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errmsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        	</section>
	}
}

export default class HousingFund extends Component {

	constructor(props) {
        super(props);
    }

	render(){
		if(this.props.match.params.in){
			return <Write/>
		}
		return <List/>
	}
}
