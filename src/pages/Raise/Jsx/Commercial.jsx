import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import autosize from 'autosize'
import Modal from 'react-modal'
import ModalImg from 'react-modal'
import ModalSelectPlace from 'react-modal'
import Loading from '../../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Commercial'
import store from '../Stores/Commercial'

import '../Scss/Commercial.scss'

export default class Commercial extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            photo:null,
            modalPlaceIsOpen:false,
            title:'商户资质证明',
            swiper:null,
            modalImgIsOpen:false,
            imgHeight:0,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        Actions.getUserauths();
        Actions.getProvince();
        localStorage.raiseRight = false;
    }

	componentDidMount() {
	    __event__.setHeader({
            title: this.state.title,
            backBtn:true,
        });
        this.setState({
            imgHeight: $(window).height() - $('header').height(),
        });
        autosize(this.refs.textarea);
	}

	openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

	myphoto(){
		const file = this.refs.file;
		if(file.files.length){
            Actions.getSignature(file.files);
		}
	}

    openModalPlace(){
        this.setState({modalPlaceIsOpen:true});
        const sw =  new Swiper('.swiper-container',{
            swipeHandler : 'none',
            effect : 'coverflow',
            slidesPerView: 1,
            centeredSlides: true,
            coverflow: {
                rotate: 30,
                stretch: 10,
                depth: 60,
                modifier: 2,
                slideShadows : true
            },
            speed:200,
        });

        this.setState({swiper:sw});

        __event__.setHeader({
            title: this.state.title,
            backBtn:false,
            raiseRight:false,
        });

        let moveTop;
        $('.bankList').on('touchstart',e=>{
            moveTop = e.touches[0].pageY + $('.bankList').scrollTop();
        });
        $('.bankList').on('touchmove',e=>{
            e.preventDefault();
            let moveNow = e.touches[0].pageY;
            $('.bankList').scrollTop(moveTop - moveNow);
        });
    }

    closeModalPlace(){
        this.setState({modalPlaceIsOpen:false});
        this.componentDidMount();
    }

    SelectCity(province){
        Actions.getCity(province);
        this.state.swiper.slideNext();
        $('.bankList').scrollTop(0);
    }

    prevModalPlace(){
        this.state.swiper.slidePrev();
        $('.bankList').scrollTop(0);
    }

    selectCounty(city){
        Actions.getCounty(city);
        this.state.swiper.slideNext();
        $('.bankList').scrollTop(0);
    }

    selectPlace(county){
        this.setState({
            county:county.name,
            regionCode:county.code,
        });
        this.closeModalPlace();
    }

    openModalImg(){
        this.setState({modalImgIsOpen:true})
    }

    closeModalImg() {
        this.setState({modalImgIsOpen:false});
    }

    toSubmit(){
        const [photo,num,regionCode,goodplace] = 
        [this.state.m.imageSend,
        this.refs.num.value,
        this.state.regionCode,
        this.refs.textarea.value];

        let err;
        if(!photo){
            err = '请拍照上传您的营业执照原件';
        }else if(!num){
            err = '请输入店铺的营业执照注册号';
        }else if(!regionCode){
            err = '请选择店铺所在地区';
        }else if(!goodplace){
            err = '请输入详细地址';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err
            });
        }else{
            Actions.companies({
                address:goodplace,
                businessLicence:num,
                regionCode:regionCode,
            });
        }
    }

    render() {
        const [st,provinces,cities,county] = [this.state.m,[],[],[]];

        st.provinces.forEach((itm,ind)=>provinces.push(<dd key={`province_${ind}`} onClick={this.SelectCity.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));
        
        st.cities.forEach((itm,ind)=>cities.push(<dd key={`city_${ind}`} onClick={this.selectCounty.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

        st.county.forEach((itm,ind)=>county.push(<dd key={`county_${ind}`} onClick={this.selectPlace.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));
       	
        return <section className='safety health commercial edu'>
                <dl className='operate'>
                	<dd>
                        <span>请拍照上传您的营业执照原件</span>
                        <label className='off'/>
	                	<img onClick={this.openModalImg.bind(this)}
                            style={{opacity:(this.state.m.imageSend?'':0)}}
                            src={this.state.m.imageSend} alt=""/>
	                	<input onChange={this.myphoto.bind(this)} ref='file' id='file' type="file" accept="image/*"/>
	                	<a/>
	                </dd>
                    <dd>
                        <span>执照注册号</span>
                        <input ref='num' placeholder='请输入店铺的营业执照注册号' type="text"/>
                    </dd>
                    <dd className='rightSvg' onClick={this.openModalPlace.bind(this)}>
                        <span>店铺地址</span>
                        <label className={this.state.county?'':'off'}>{this.state.county?`${st.sProvince} ${st.sCity} ${this.state.county}`:'请选择店铺所在地区'}</label>
                        <Arrow fill='#3366cc'/>
                    </dd>
                </dl>
                <div className='autotextarea'>
                    <textarea ref='textarea' placeholder='请输入详细地址'>
                    </textarea>
                </div>
                
                <a onClick={this.toSubmit.bind(this)} 
                    className={`btn_abs_bottom android_${this.state.anDroid}`} 
                    href="javascript:;">
                    提交
                </a>

                <ModalSelectPlace
                    isOpen={this.state.modalPlaceIsOpen}
                    onAfterOpen={this.openModalPlace.bind(this)}
                    onRequestClose={this.closeModalPlace.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'#eee',
                        }
                    }}
                >   
                    <div className="swiper-container selectCity">
                        <div ref='swiperWrapper' className="swiper-wrapper rightSvg">
                            <div className="swiper-slide">
                                <p className='cityTitle citylist' onClick={this.closeModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/></p>
                                <dl className='bankList citylist'>
                                    {provinces}<dd></dd>
                                </dl>
                            </div>
                            <div className="swiper-slide">
                                <p className='cityTitle citylist' onClick={this.prevModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/>
                                    <span>{st.sProvince}</span>
                                </p>
                                <dl style={{height:this.state.cityHeight}} className='rightSvg bankList citylist'>
                                    {cities}<dd></dd>
                                </dl>
                            </div>
                            <div className="swiper-slide">
                                <p className='cityTitle citylist' onClick={this.prevModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/>
                                    <span>{st.sProvince} {st.sCity}</span>
                                </p>
                                <dl className='rightSvg bankList citylist'>
                                    {county}<dd></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </ModalSelectPlace>

                <ModalImg
                    isOpen={this.state.modalImgIsOpen}
                    onAfterOpen={this.openModalImg.bind(this)}
                    onRequestClose={this.closeModalImg.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:$(window).width(),
                            height:this.state.imgHeight,
                            background: '0 0',
                            textAlign:'center',
                            border:0,
                            display:'-webkit-flex',
                        }
                    }}
                >   
                    <a className='close' onClick={this.closeModalImg.bind(this)} href="javascript:;"></a>
                    <img src={this.state.m.imageSend}/>
                </ModalImg>

                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content'>
                        <i onClick={this.closeModal.bind(this)}>&times;</i>
                        {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                    </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Commercial, Reflux.connect(store,'m'));