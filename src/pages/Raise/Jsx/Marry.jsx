import React,{Component} from  'react'
import Modal from 'react-modal'
import ModalImg from 'react-modal'
import ModalSelect from 'react-modal'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import Loading from '../../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Marry'
import store from '../Stores/Marry'

import '../Scss/Marry.scss'

export default class Marry extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            report:null,
            modalSelectIsOpen:false,
            marry:'',
            modalImgIsOpen:false,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        Actions.getUserextauths();
        localStorage.raiseRight = true;
    }
 
	componentDidMount() {
	    __event__.setHeader({
            title: '婚姻状况',
            backBtn:true,
        });
	}

	openModalSelct(){
        this.setState({modalSelectIsOpen:true})
    }

    closeModalSelect() {
        this.setState({modalSelectIsOpen:false})
    }

	openModal(){
        this.setState({modalIsOpen:true})
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    openModalImg(){
        this.setState({modalImgIsOpen:true})
    }

    closeModalImg() {
        this.setState({modalImgIsOpen:false})
    }

	myReport(e){
		const file = e.target;
		if(file.files.length){
            Actions.getSignature(file.files);
		}
	}

	SelectMarry(msg){
		this.setState({marry:msg});
		this.closeModalSelect();
	}

    toSubmit(){
        const [mariStatus,report] = [this.state.marry,this.state.m.imageSend];
        let err;
        if(!mariStatus){
            err = '请选择婚姻状况';
        }else if(mariStatus === '已婚'&&!report){
            err = '请上传结婚证';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }else{
            Actions.marital({mariStatus});
        }
    }

    render() {
    	const st = this.state.m;
       	return <section className='safety marry'>
                <dl className='operate'>
                	<dt className='rightSvg' onClick={this.openModalSelct.bind(this)}>
                		<span>婚姻状况</span>
                		{this.state.marry}
                		<Arrow fill='#3366cc'/>
                	</dt>
                    <dd>
                        <span>上传结婚证</span>
                        <a href='javascript:;' hidden>查看范例</a>
                    </dd>
                	<dd>
                        <span>点击上传结婚证</span>
	                	<img onClick={this.openModalImg.bind(this)} 
                        style={{display:(this.state.m.imageSend?'':'none')}} 
                        src={this.state.m.imageSend}/>
	                	<input onChange={this.myReport.bind(this)} 
                        ref='file' id='file' type="file" accept='image/*'/>
	                	<a></a>
	                </dd>
                </dl>
                <p>注：若为“已婚”,需上传结婚证照片</p>
                <a className={`btn_abs_bottom android_${this.state.anDroid}`} 
                 href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>
                <ModalSelect
                    isOpen={this.state.modalSelectIsOpen}
                    onAfterOpen={this.openModalSelct.bind(this)}
                    onRequestClose={this.closeModalSelect.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content select'>
                  	  <i onClick={this.closeModalSelect.bind(this)}>&times;</i>请选择婚姻状况
                  </div>
	                  <dl>
	                  	<dd onClick={this.SelectMarry.bind(this,'未婚')}>未婚</dd>
	                  	<dd onClick={this.SelectMarry.bind(this,'已婚')}>已婚</dd>
	                  	<dd onClick={this.SelectMarry.bind(this,'离异')}>离异</dd>
	                  </dl>
                </ModalSelect>

                <ModalImg
                    isOpen={this.state.modalImgIsOpen}
                    onAfterOpen={this.openModalImg.bind(this)}
                    onRequestClose={this.closeModalImg.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            background: 'none',
                            textAlign:'center',
                            border:0,
                            display:'-webkit-flex',
                            position:'fixed',
                        }
                    }}
                >
                    <a className='close' onClick={this.closeModalImg.bind(this)} href="javascript:;"></a>
                    <img onClick={this.closeModalImg.bind(this)}  src={this.state.m.imageSend}/>
                </ModalImg>

                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content'>
                        <i onClick={this.closeModal.bind(this)}>&times;</i>
                        {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                    </div>
                    <div className='btn' onClick={this.closeModal.bind(this)}>
                        <a href="javascript:;">我知道了</a>
                    </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Marry, Reflux.connect(store,'m'));