import React,{Component} from  'react'
import Modal from 'react-modal'
import Loading from '../../../components/Loading'
import ModalDemo from 'react-modal'
import ModalImg from 'react-modal'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Certification'
import store from '../Stores/Certification'


import '../Scss/Certification.scss'

export default class Certification extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            modalDemoIsOpen:false,
            headerH:0,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        localStorage.raiseRight = false;
        Actions.getUserauths();
    }

	componentDidMount() {
	     __event__.setHeader({
            title: '实名认证',
            backBtn:true,
        });
        this.setState({
            headerH:$('header').height()
        });
	}

	openModal(){
        if(this.state.m.modalIsOpen){
            this.state.errmsg = this.state.m.errorMsg;
        }
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    componentDidUpdate() {
        const myerror = this.refs.myerror;
        if(myerror){
            if(this.state.modalIsOpen){
                $(myerror).html(myerror.innerText);
            }else{
                $(myerror).html(myerror.innerHtml);
            }
        }
    }

	toSubmit(){
		const [name,idcard,file_on,file_off,file_hand] = [this.refs.name.value,
                                                        this.refs.idcard.value,
                                                        this.state.m.imgs.file_on,
                                                        this.state.m.imgs.file_off,
                                                        this.state.m.imgs.file_hand
                                                        ];
		let err;
		if(!name){
			err = `请输入姓名`;
		}else if(idcard.length !== 18){
			err = '身份证号码不正确';
		}else if(!file_on){
            err = '请上传身份证正面照片';
        }else if(!file_off){
            err = '请上传身份证反面照片';
        }else if(!file_hand){
            err = '请上传手持身份证照片';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }else{
            Actions.getRealNameAuth(name,idcard);
        }
	}

    inputIDCard(e){
        const idcard = e.target;
        idcard.value = idcard.value.replace(/\s/g, "");
        this.hideClearInput('idcard');
    }

    lookDemo(){
        this.setState({modalDemoIsOpen:true});
    }

    openDemoModal(){
         new Swiper('.swiper-container', {
            pagination : '.swiper-pagination',
            slidesPerView : 'auto',
            slidesPerView : 1.5,
            spaceBetween : 20,
            centeredSlides : true,
            paginationClickable :false,
        });
    }

    closeDemoModal(){
        this.setState({modalDemoIsOpen:false});
    }

    addImage(e){
        const file = e.target;
        if(file.files.length){
            Actions.getSignature(file,file.name);
        }
    }

    imgRemove(e){
        Actions.imgRemove(e.target.getAttribute('name'));
    }

    openModalImgBefore(img){
        let mimg = new Image();
        mimg.src = img;
        this.setState({
            modalImgIsOpen:true,
            report:img,
        })
    }

    openModalImg(){}

    closeModalImg() {
        this.setState({modalImgIsOpen:false})
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        $('header').css('position', 'absolute');
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render() {
    	const st = this.state.m;
       	return <section className='safety'>
                    <dl id='certification' className='operate'>
                    	<dd>
                            <span>姓名</span>
                    		<input onFocus={this.showClearInput.bind(this,'name')} 
                            onBlur={this.hideClearInput.bind(this,'name')} 
                            ref='name' maxLength='8' placeholder='请输入您的真实姓名' type="text"/>
                    	    <a className='blueClearInput' 
                            onClick={this.clearInput.bind(this,'name')} href="javascript:;">&times;</a>
                        </dd>
                    	<dd>
                            <span>身份证号</span>
    	                	<input onFocus={this.showClearInput.bind(this,'idcard')} 
                            ref='idcard' maxLength = '18' onBlur={this.inputIDCard.bind(this)} placeholder='请输入您的18位身份证号' type="text"/>
                            <a className='blueClearInput' 
                            onClick={this.clearInput.bind(this,'idcard')} href="javascript:;">&times;</a>
                        </dd>
                    </dl>
                    <div className='photo'>
                        请上传您手持身份证照片
                        <a href='javascript:;' onClick={this.lookDemo.bind(this)}>查看范例</a>
                        <dl className='certification'>
                            <dd className={st.imgs.file_on?'on':''} style={{backgroundImage:`url(${st.imgs.file_on})`}}>
                                <input name='file_on' style={{display:(st.imgs.file_on?'none':'')}} 
                                onChange={this.addImage.bind(this)} 
                                type="file" ref='file_on' accept="image/*"/>
                                <i name='file_on' onClick={this.imgRemove.bind(this)}>&times;</i>
                                <label>+</label>
                                <em>身份证正面</em>
                                <a onClick={this.openModalImgBefore.bind(this,st.imgs.file_on)} 
                                href="javascript:;"/>
                            </dd>
                            <dd className={st.imgs.file_off?'on':''} style={{backgroundImage:`url(${st.imgs.file_off})`}}>
                                <input name='file_off' style={{display:(st.imgs.file_off?'none':'')}} 
                                onChange={this.addImage.bind(this)} type="file" ref='file_off' accept="image/*"/>
                                <i name='file_off' onClick={this.imgRemove.bind(this)}>&times;</i>
                                <label>+</label>
                                <em>身份证反面</em>
                                <a onClick={this.openModalImgBefore.bind(this,st.imgs.file_off)} href="javascript:;"/>
                            </dd>
                            <dd className={st.imgs.file_hand?'on':''} style={{backgroundImage:`url(${st.imgs.file_hand})`}}>
                                <input name='file_hand' style={{display:(st.imgs.file_hand?'none':'')}} 
                                onChange={this.addImage.bind(this)} 
                                type="file" ref='file_hand' accept="image/*"/>
                                <i name='file_hand' onClick={this.imgRemove.bind(this)}>&times;</i>
                                <label>+</label>
                                <em>手持身份证</em>
                                <a onClick={this.openModalImgBefore.bind(this,st.imgs.file_hand)} href="javascript:;"/>
                            </dd>
                        </dl>
                    </div>
                    <a className={`btn_abs_bottom android_${this.state.anDroid}`} href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>

                    <ModalImg
                        isOpen={this.state.modalImgIsOpen}
                        onAfterOpen={this.openModalImg.bind(this)}
                        onRequestClose={this.closeModalImg.bind(this)}
                        closeTimeoutMS={150}
                        style={{
                            overlay : {
                                top:$('header').height()
                            },
                            content : {
                                width:$(window).width(),
                                height:$(window).height() - this.state.headerH,
                                background: 'none',
                                border:0,
                                textAlign:'center',
                                display:'-webkit-flex',
                            }
                        }}
                    >
                    <a className='close' onClick={this.closeModalImg.bind(this)}  href="javascript:;"></a>
                    <img src={this.state.report}/>
                </ModalImg>

                <Modal
                    isOpen={this.state.modalIsOpen||st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content'>
                        <i onClick={this.closeModal.bind(this)}>&times;</i>
                        <span ref='myerror'>{st.modalIsOpen?st.errorMsg:this.state.errmsg}</span>
                    </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>

                <ModalDemo
                    isOpen={this.state.modalDemoIsOpen}
                    onAfterOpen={this.openDemoModal.bind(this)}
                    onRequestClose={this.closeDemoModal.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:this.state.headerH
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'rgba(102,102,102,0.6)',
                        }
                    }}
                >
                    <div onClick={this.closeDemoModal.bind(this)} className="swiper-container swiperPhotoDemo">
                        <div className="swiper-wrapper">
                            <div className="swiper-slide item_1">
                            </div>
                            <div className="swiper-slide item_2">
                            </div>
                            <div className="swiper-slide item_3">
                            </div>
                        </div>
                        <div className="swiper-pagination"></div>
                    </div>
                </ModalDemo>
                <Loading type={st.loading}/>
            </section>
    }
};

ReactMixin.onClass(Certification, Reflux.connect(store,'m'));