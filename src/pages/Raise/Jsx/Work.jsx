import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'
import ModalSelect from 'react-modal'
import ModalTip from '../../../components/ModalTip'

import '../Scss/Work.scss'

export default class Work extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            modalSelectIsOpen:false,
            income:'',
            tipNum:0
        }
    }

    componentDidMount() {
	     __event__.setHeader({
            title: '工作信息',
            backBtn:true,
            raiseRight:true
        });
	}


	openModalSelect(){
        this.setState({modalSelectIsOpen:true});
    }

    closeModalSelect() {
        this.setState({modalSelectIsOpen:false});
    }

	openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        this.setState({modalIsOpen:false});
    }

	toSubmit(){
		const [name,email,income] = [this.refs.name.value,this.refs.email.value,this.state.income];
		let err;
		const emailReg = new RegExp(/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/);
		if(!name){
			err = '请输入您所在公司的名称';
		}else if(!emailReg.test(email)){
			err = '请输入正确的邮箱地址';
		}else if(!income){
			err = '请选择您的工资水平';
		}

		if(err){
			this.setState({
				modalIsOpen:true,
				errmsg:err
			})
		}
	}

	selectIncome(){
		this.setState({modalSelectIsOpen:true});
	}

	setIncome(income){
		this.setState({
			income:income,
			modalSelectIsOpen:false
		});
	}

	changeEmail(){
		const [emailReg,email] = [new RegExp(/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/),this.refs.email.value];
		if(emailReg.test(email)){
			this.setState({
                tipNum:++this.state.tipNum
            });
		}else{
			this.setState({
				modalIsOpen:true,
				errmsg:'请输入正确的邮箱地址'
			});
		}
		
	}

    render() {
    	const st = this.state;
    	return <section className='payment work'>
    		<dl className='rightSvg'>
                    <dd>公司名称
                    	<input ref='name' type="text" placeholder='请输入您所在公司的名称'/>
                    </dd>
                    <dd>邮箱验证
                    	<input ref='email' type="email" placeholder='请输入您的邮箱'/>
                    	<a onClick={this.changeEmail.bind(this)} href="javascript:;">更换邮箱</a>
                    </dd>
                    <dd>工资水平
                    	<input value={st.income} onClick={this.selectIncome.bind(this)} ref='income' type="text" placeholder='请选择您的工资水平' readOnly/>
                    	<Arrow fill='#3366cc'/>
                    </dd>
                </dl>
                <a onClick={this.toSubmit.bind(this)} href="javasctipt:;" className="btn_abs_bottom">提交</a>

    			<ModalSelect
                    isOpen={st.modalSelectIsOpen}
                    onAfterOpen={this.openModalSelect.bind(this)}
                    onRequestClose={this.closeModalSelect.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content select'>
                  	  <i onClick={this.closeModalSelect.bind(this)}>&times;</i>请选择工资水平
                  </div>
	                  <dl>
	                  	<dd onClick={this.setIncome.bind(this,'3000元以下')}>3000元以下</dd>
	                  	<dd onClick={this.setIncome.bind(this,'3000-5000')}>3000-5000</dd>
	                  	<dd onClick={this.setIncome.bind(this,'5000-7000')}>5000-7000</dd>
	                  	<dd onClick={this.setIncome.bind(this,'7000-9000')}>7000-9000</dd>
	                  </dl>
                </ModalSelect>

                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                <div className='content'>
                  	<i onClick={this.closeModal.bind(this)}>&times;</i>
                  	{st.errmsg}
                </div>
                <div className='btn' onClick={this.closeModal.bind(this)}>
                    <a href="javascript:;">我知道了</a>
                </div>
            </Modal>
            <ModalTip msg='我们已向您的邮箱账号发送了验证邮件，请按邮件内提示完成验证操作。' tipNum={this.state.tipNum}/>
    	</section>
   }
}