import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import CountDown from '../../../components/CountDown'
import Modal from 'react-modal'
import ModalSelectBank from 'react-modal'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/BankWater'
import store from '../Stores/BankWater'

import '../Scss/BankWater.scss'

export default class BankWater extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            codeTxt:'发送验证码',
            modalBankIsOpen:true,
            bankName:'',
            agree:true,
        }
        Actions.getBankList();
    }

	componentDidMount() {
	    __event__.setHeader({
            title: '选择银行',
            backBtn:true,
            raiseRight:true
        });
	}

    openModalBank(){
        this.setState({modalBankIsOpen:true});
    }

    closeModalBank() {
        this.setState({modalBankIsOpen:false});
    }

	openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        this.setState({modalIsOpen:false});
    }

    toSubmit(){
        const [bankName,card,mobile,code] = [this.state.bankName,
        this.refs.card.value,
        this.refs.mobile.value,
        this.refs.code.value];
        let err;
        if(!bankName){
            err = '请选择发卡银行';
        }else if(!card){
            err = '请输入银行卡号';
        }else if(mobile.length < 11){
            err = '请输入正确的的预留号码';
        }else if(!code){
            err = '请输入验证码';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }
    }

    bankChoose(bank){
        this.setState({
            bankName:bank.bankName,
            modalBankIsOpen:false,
        });
        __event__.setHeader({
            title: bank.bankName,
            backBtn:true,
            raiseRight:true,
        });
    }

	toAgree(){
		this.setState({agree:!this.state.agree});
	}

    selectBtn(cardType){
    	$('.select').removeClass('select');
    	this.refs[cardType].className = 'select';
    }

    render() {
        const st = this.state.m;
        let banks = [];

        st.bankInfos.forEach((itm,ind)=>banks.push(<dd key={`banks_${ind}`} onClick={this.bankChoose.bind(this,itm)}><i className={`s_${itm.bankCode}`}></i>{itm.bankName}<Arrow fill='#3366cc'/></dd>));


       	return <section className='safety bankwater'>
       			<ul>
       				<li ref='saving' onClick={this.selectBtn.bind(this,'saving')} className='select'>储蓄卡</li>
       				<li ref='credit' onClick={this.selectBtn.bind(this,'credit')}>信用卡</li>
       			</ul>
            	<dl className='operate'>
                	<dd><span>卡号</span>
	                	<input ref='card' placeholder='请输入完整卡号' type="text"/>
	                </dd>
	                <dd><span>密码</span>
	                	<input ref='password' maxLength = '6' placeholder='请输入查询密码' type="password"/>
	                </dd>
                </dl>
                <em>注意：输入您的6位查询密码，忘记密码请至官网找回</em>
        		<p id='box_check'><i onClick = {this.toAgree.bind(this)} className={this.state.agree?'on':''}></i>我已阅读并同意 《***协议》</p>


                <a onClick={this.toSubmit.bind(this)} className='btn_abs_bottom' href="javascript:;">提交</a>
                <ModalSelectBank
                    isOpen={this.state.modalBankIsOpen}
                    onAfterOpen={this.openModalBank.bind(this)}
                    onRequestClose={this.closeModalBank.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'#eee',
                        }
                    }}
                >
                    <dl id='bankwater' className='rightSvg bankList'>
                        {banks}
                    </dl>
                </ModalSelectBank>

                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                    {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(BankWater, Reflux.connect(store,'m'));