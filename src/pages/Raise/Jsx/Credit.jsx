import React,{Component} from  'react'
import Modal from 'react-modal'
import Loading from '../../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Credit'
import store from '../Stores/Credit'

import '../Scss/Credit.scss'

export default class Credit extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            date:Date.now(),
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        Actions.getUserextauths();
        localStorage.raiseRight = true;
    }

	componentDidMount() {
	     __event__.setHeader({
            title: '人行征信认证',
            backBtn:true,
        });
	}

	openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

	toSubmit(){
		const [name,psd,code,imgCode] = [this.refs.name.value,this.refs.psd.value,this.refs.code.value,this.refs.imgCode.value];
		let err;
		if(!name){
			err = '请输入您的真实姓名';
		}else if(!psd){
			err = '请输入您的征信账号登录密码';
		}else if(!code){
            err='请输入您的身份验证码';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }else{
            Actions.pbc({
                userName:name,
                password:psd,
                verifyCode:code,
            });
        }
	}

    changeImgCode(){
        this.setState({date:Date.now()})
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        $('header').css('position', 'absolute');
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render() {
    	const st = this.state.m;
        // const imgSrc = `//121.43.148.191:8125/randcode/randCode_v2.json?mobile=${st.user.mobileOri}&time=${this.state.date}`;
        const imgSrc = '';

       	return <section className='safety credit'>
                <dl className='operate'>
                	<dd><span>登录名</span>
                		<input onFocus={this.showClearInput.bind(this,'name')} 
                            onBlur={this.hideClearInput.bind(this,'name')} 
                            ref='name' maxLength='10' placeholder='请输入您的真实姓名' type="text"/>
                        <a className='blueClearInput' 
                        onClick={this.clearInput.bind(this,'name')} href="javascript:;">&times;</a>
                    </dd>
                	<dd><span>登录密码</span>
	                	<input maxLength='30' onFocus={this.showClearInput.bind(this,'psd')} 
                            onBlur={this.hideClearInput.bind(this,'psd')} 
                            ref='psd' placeholder='请输入您的征信账号登录密码' type="password"/>
                        <a className='blueClearInput' 
                        onClick={this.clearInput.bind(this,'psd')} href="javascript:;">&times;</a>
	                </dd>
                    <dd><span>身份验证码</span>
                        <input maxLength='30' onFocus={this.showClearInput.bind(this,'code')} 
                            onBlur={this.hideClearInput.bind(this,'code')} 
                            ref='code' placeholder='请输入您的身份验证码' type="text"/>
                        <a className='blueClearInput' 
                        onClick={this.clearInput.bind(this,'code')} href="javascript:;">&times;</a>
                    </dd>
                    <dd style={{display:'none'}}><span>验证码</span>
                        <input ref='imgCode' placeholder='请输入右侧图片验证码' type="text"/>
                        <img onClick={this.changeImgCode.bind(this)} src={imgSrc} alt=""/>
                    </dd>
                </dl>
                <a className={`btn_abs_bottom android_${this.state.anDroid}`} 
                 href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>
                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                        {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                    </div>
                    <div className='btn' onClick={this.closeModal.bind(this)}>
                        <a href="javascript:;">我知道了</a>
                    </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Credit, Reflux.connect(store,'m'));