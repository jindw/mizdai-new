import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import Modal from 'react-modal'

import '../Scss/PayMent.scss'

class Main extends Component {

    toMobile(){
        location.hash = '#/raise/payment/mobile'
    }

    render() {
        const st = this.state;
        return <section className='payment'>
                <dl className='rightSvg'>
                    <dd onClick={this.toMobile}>手机缴费记录<Arrow fill='#3366cc'/></dd>
                    <dd>电费缴费记录<Arrow fill='#3366cc'/></dd>
                </dl>
        </section>
    }
};

class Mobile extends Component {

    constructor(props) {
        super(props);
        this.state={
            modalIsOpen:false,
            errmsg:''
        }
    }

    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        this.setState({modalIsOpen:false});
    }

    toSubmit(){
        const [mobile,psd] = [this.refs.mobile.value,this.refs.psd.value];
        let err;
        if(mobile.length < 11){
            err = '请输入正确的手机号码';
        }else if(psd){
            err = '请输入服务密码';
        }

        err&&this.setState({
            modalIsOpen:true,
            errmsg:err
        });
    }

    render() {
        const st = this.state;
        return <section className='payment'>
                <dl>
                    <dt>手机缴费记录</dt>
                    <dd>手机号码<input ref='mobile' maxLength='11' type="tel" placeholder='请输入您的手机号码'/></dd>
                    <dd>服务密码<input ref='psd' maxLength='20' type="text" placeholder='请输入您的手机服务密码'/></dd>
                </dl>
                <p>服务密码请向您的运营商进行咨询</p>
                <a onClick={this.toSubmit.bind(this)} href="javasctipt:;" className="btn_abs_bottom">提交</a>
                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errmsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

export default class PayMent extends Component {

	constructor(props) {
        super(props);
    }

	componentDidMount() {
	     __event__.setHeader({
            title: '缴费记录',
            backBtn:true,
            raiseRight:true
        });
	}

    render() {
        if(this.props.match.params.in === 'mobile'){
            return <Mobile/>
        }
       	return <Main/>
    }
};
