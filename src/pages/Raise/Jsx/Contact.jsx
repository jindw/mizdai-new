import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import ModalTip from '../../../components/ModalTip'
import Loading from '../../../components/Loading'
import ModalSelect from 'react-modal'
import Modal from 'react-modal'
import _times from 'lodash/times'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Contact'
import store from '../Stores/Contact'

import '../Scss/Contact.scss'

export default class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tipNum:1,
            connectref:null,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        localStorage.raiseRight = false;
        Actions.getUserauths();
    }

    componentDidMount() {
         __event__.setHeader({
            title: '添加联系人',
            backBtn:true,
        });
    }

    openModalSelect(){
        $('.ReactModal__Overlay').on('touchmove', e=> e.preventDefault());
    }

    closeModalSelect() {
        this.setState({modalSelectIsOpen:false});
    }

    selectConnect(conref){
        setTimeout(()=>{
            this.setState({
                connectref:conref,
                modalSelectIsOpen:true
            })
        },300);
    }

    whiteConnect(conName,conn,straight){
        const thecon = this.refs[this.state.connectref];
        thecon.value = conName;
        $(thecon).data('conn',conn).removeClass('straight').addClass(`${straight}`);
        this.setState({modalSelectIsOpen:false});
    }
    openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    toSubmit(){
        const [c1,c1name,c1mobile,
            c2,c2name,c2mobile,
            c3,c3name,c3mobile
            ] = 
            [$(this.refs.connect_1).data('conn'),
            this.refs.connect_1_name.value,
            this.refs.connect_1_mobile.value,

            $(this.refs.connect_2).data('conn'),
            this.refs.connect_2_name.value,
            this.refs.connect_2_mobile.value,

            $(this.refs.connect_3).data('conn'),
            this.refs.connect_3_name.value,
            this.refs.connect_3_mobile.value
            ];
        let [err,straight] = [,false];

        const contacts = [];
        if(!c1){
            err = '请选择与常用联系人1的关系';
        }else if(!c1name){
            err = '请输入与常用联系人1的姓名';
        }else if(!(/^1[3|4|5|7|8]\d{9}$/.test(c1mobile))){
            err = '请正确输入与常用联系人1的手机号';
        }else{
            contacts.push({
                mobile:c1mobile,
                name:c1name,
                relationship:c1
            })
        }

        if(!err){
            if(!c2){
                err = '请选择常用联系人2的关系';
            }else if(!c2name){
                err = '请输入常用联系人2的姓名';
            }else if(!(/^1[3|4|5|7|8]\d{9}$/.test(c2mobile))){
                err = '请正确输入常用联系人2的手机号';
            }else{
                contacts.push({
                    mobile:c2mobile,
                    name:c2name,
                    relationship:c2
                })
            }
        }

        if(!err){
            if(!c3){
                err = '请选择常用联系人3的关系';
            }else if(!c3name){
                err = '请输入常用联系人3的姓名';
            }else if(!(/^1[3|4|5|7|8]\d{9}$/.test(c3mobile))){
                err = '请正确输入常用联系人3的手机号';
            }else{
                contacts.push({
                    mobile:c3mobile,
                    name:c3name,
                    relationship:c3
                })
            }
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err
            });
        }else{
            Actions.addContacts(contacts);
        }
    }

    mobileIn(mob){
        const mobile = this.refs[mob];
        const newmobile = mobile.value.replace(/^0+|[^0-9]/g, "");
        mobile.value = newmobile.substring(0,11);
    }

    clearInput(input){
        this.refs[input].value = '';
        this.refs[input].focus();
    }

    showClearInput(input){
        $(this.refs[input]).addClass('showclear');
        $('header').css('position', 'absolute');
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    render() {
        const [st,list] = [this.state.m,[]];
        _times(3,index=>{
            const ind = ++index;
            list.push(<dl key={`list_${ind}`}>
                <dt>常用联系人{ind}</dt>
                <dd onClick={this.selectConnect.bind(this,`connect_${ind}`)} className='rightSvg'>
                    <span>关系</span>
                    <input className='connects' ref={`connect_${ind}`} type="text" placeholder='请选择与联系人关系' readOnly/>
                    
                    <Arrow fill='#3366cc'/>
                </dd>
                <dd>
                    <span>姓名</span>
                    <input onFocus={this.showClearInput.bind(this,`connect_${ind}_name`)} 
                            onBlur={this.hideClearInput.bind(this,`connect_${ind}_name`)}
                             maxLength='8' ref={`connect_${ind}_name`} type="text" placeholder='请输入联系人姓名'/>
                    <a className='blueClearInput' 
                        onClick={this.clearInput.bind(this,`connect_${ind}_name`)} href="javascript:;">&times;</a>
                </dd>
                <dd>
                    <span>手机号</span>
                    <input onFocus={this.showClearInput.bind(this,`connect_${ind}_mobile`)} 
                            onBlur={this.hideClearInput.bind(this,`connect_${ind}_mobile`)}
                             onChange={this.mobileIn.bind(this,`connect_${ind}_mobile`)}
                             ref={`connect_${ind}_mobile`} type="tel" placeholder='请输入联系人手机号'/>
                    <a className='blueClearInput' 
                        onClick={this.clearInput.bind(this,`connect_${ind}_mobile`)} href="javascript:;">&times;</a>
                </dd>
            </dl>);
        })
    	return <section className="contact">
            {list}
            <a onClick={this.toSubmit.bind(this)} href="javascript:;" 
            className={`btn_abs_bottom android_${this.state.anDroid}`}>提交</a>
            <Modal
                isOpen={st.modalIsOpen||this.state.modalIsOpen}
                onAfterOpen={this.openModal.bind(this)}
                onRequestClose={this.closeModal.bind(this)}
                closeTimeoutMS={150}
            >
              <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                {st.modalIsOpen?st.errorMsg:this.state.errmsg}</div>
              <div className='btn' onClick={this.closeModal.bind(this)}>
                  <a href="javascript:;">我知道了</a>
              </div>
            </Modal>
            <ModalSelect
                isOpen={this.state.modalSelectIsOpen}
                onAfterOpen={this.openModalSelect.bind(this)}
                onRequestClose={this.closeModalSelect.bind(this)}
                closeTimeoutMS={150}
            >
                <div ref='contact' id='contact' className='content select'>
                    <i onClick={this.closeModalSelect.bind(this)}>&times;</i>
                        请选择与联系人关系
                    <em>（须至少包含一位直系亲属）</em>
                </div>
                <dl>
                    <dd onClick={this.whiteConnect.bind(this,'配偶','Spouse','straight')}>配偶<span>（直系）</span></dd>
                    <dd onClick={this.whiteConnect.bind(this,'父母','Parent','straight')}>父母<span>（直系）</span></dd>
                    <dd onClick={this.whiteConnect.bind(this,'子女','Child','straight')}>子女<span>（直系）</span></dd>
                    <dd onClick={this.whiteConnect.bind(this,'亲戚','Relative','')}>亲戚</dd>
                    <dd onClick={this.whiteConnect.bind(this,'朋友','Friend','')}>朋友</dd>
                </dl>
            </ModalSelect>
            <ModalTip msg='请添加常用联系人。如您未按时还款，且我们无法与您取得联系时，我们会通知您的常用联系人' tipNum={this.state.tipNum}/>
            <Loading type={st.loading}/>
        </section>
   }
};

ReactMixin.onClass(Contact, Reflux.connect(store,'m'));