import React,{Component} from  'react'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import CountDown from '../../../components/CountDown'
import Modal from 'react-modal'
import ModalSelectBank from 'react-modal'
import Loading from '../../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Bank'
import store from '../Stores/Bank'

import '../Scss/Bank.scss'

export default class Bank extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            codeTxt:'发送验证码',
            modalBankIsOpen:false,
            bankName:'',
            countDownFalse:'请选择发卡银行',
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
            minHeight:0,
        }
        Actions.getBankList();
        this.beforeSendCode = this.beforeSendCode.bind(this);
        localStorage.raiseRight = false;
        Actions.getUserauths();
    }

	componentDidMount() {
	    __event__.setHeader({
            title: '银行卡认证',
            backBtn:true,
        });
        this.setState({
            minHeight: $(window).height() - $('header').height(),
        });

        $('#countDown').on('click',() => {
            if(!$('#countDown.off').length){
                this.beforeSendCode();
            }
        });

        new Cleave('#bankCard', {
            blocks: [4,4,4,4,4],
        });
	}

    beforeSendCode(){
        const [bankCode,card,mobile] = [this.state.bankCode,this.refs.card.value,this.refs.mobile.value];
        let err;
        if(!bankCode){
            err = '请选择发卡银行';
        }else if(!card){
            err = '请输入银行卡号';
        }else if(!(/^1[3|4|5|7|8]\d{9}$/.test(mobile))){
            err = '请输入正确的手机号码';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }else{
            this.refs.beforeSendCode.focus();
            Actions.sendBindvalidateCode({
                cardNumber:card.replace(/^0+|[^0-9]/g, ""), 
                mobile:mobile,
                bankCode:bankCode,
            });
        }
    }

    openModalBank(){
        if(!this.state.m.codeOk){
            this.setState({modalBankIsOpen:true});
        }
        let moveTop;
        $('.bankList').on('touchstart',e=>{
            moveTop = e.touches[0].pageY + $('.bankList').scrollTop();
        });
        $('.bankList').on('touchmove',e=>{
            e.preventDefault();
            let moveNow = e.touches[0].pageY;
            $('.bankList').scrollTop(moveTop - moveNow);
        });

         __event__.setHeader({
            title: '选择银行卡',
            backBtn:false,
        });
    }

    closeModalBank() {
        this.setState({modalBankIsOpen:false});
        __event__.setHeader({
            title: '银行卡认证',
            backBtn:true,
        });
    }

	openModal(){
        this.setState({modalIsOpen:true});
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }
        if(this.state.m.modalIsOpen){
            Actions.closeModal();
        }
        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
    }

    toSubmit(){
        const [bankName,card,mobile,code,bankCardId] = 
        [this.state.bankName,
        this.refs.card.value.replace(/^0+|[^0-9]/g, ""),
        this.refs.mobile.value,
        this.refs.code.value,
        this.state.m.bankCardId
        ];
        let err;
        if(!bankName){
            err = '请选择发卡银行';
        }else if(!card){
            err = '请输入银行卡号';
        }else if(mobile.length < 11){
            err = '请输入正确的的预留号码';
        }else if(code.length < 6){
            err = '请输入正确的短信验证码';
        }else if(!bankCardId){
            err = '请先获取短信验证码';
        }

        if(err){
            this.setState({
                modalIsOpen:true,
                errmsg:err,
            });
        }else{
            Actions.confirm({
                bankCardId:bankCardId,
                mobile:mobile,
                payGateType:this.state.m.payGateType,
                validateCode:code,
            });
        }
    }

    bankChoose(bank){
        this.setState({
            bankName:bank.bankName,
            bankCode:bank.bankCode,
        });
        this.closeModalBank();
    }

    findError(){
        const [bankName,card,mobile,code] = [this.state.bankName,
        this.refs.card.value,this.refs.mobile.value,this.refs.code.value];
        if(!bankName){
            return '请选择发卡银行';
        }else if(!card){
            return '请输入银行卡号';
        }else if(mobile.length < 11){
            return '请输入正确的的预留号码';
        }
    }
    componentDidUpdate() {
        if(this.state.countDownFalse!==this.findError()){
            this.setState({countDownFalse:this.findError()});
        }
    }

    changeNum(num){
        const nums = this.refs[num];
        nums.value = nums.value.replace(/^0+|[^0-9]/g, "");
        this.componentDidUpdate();
    }

    componentWillUnmount() {
        $('#countDown').off('click');
    }

    focusMobNow(type){
        if(this.state.m.codeOk){
            $(this.refs[type]).blur();
        }else{
            $(this.refs[type]).addClass('showclear');
            $('header').css('position', 'absolute');
        }
    }

    clearInput(input){
        if(!this.state.m.codeOk){
            this.refs[input].value = '';
            this.refs[input].focus();
        }
    }

    hideClearInput(input){
        $(this.refs[input]).removeClass('showclear');
        setTimeout(()=>$('header').css('position', 'fixed'),200);
    }

    changeBankCard(e){
        e.target.value = e.target.value.replace(/[^\d\s]/g,'');
    }

    render() {
    	const [st,banks] = [this.state.m,[]];

        st.bankInfos.forEach((itm,ind)=>banks.push(<dd key={`banks_${ind}`} 
            onClick={this.bankChoose.bind(this,itm)}><i className={`s_${itm.bankCode}`}></i>{itm.bankName}<Arrow fill='#3366cc'/></dd>));

       	return <section className='safety addBank'>
                <dl className='operate'>
                	<dd onClick={this.openModalBank.bind(this)} className='rightSvg'>
                		<span>选择银行</span>
	                	<input value={this.state.bankName} ref='bankName' 
                        placeholder='请选择发卡银行' type="text" readOnly/>
                        <Arrow fill='#3366cc'/>
                	</dd>
                	<dd><span>卡号</span>
	                	<input id='bankCard'
                            onBlur={this.hideClearInput.bind(this,'card')} 
                            onChange={this.changeBankCard.bind(this)}
                             onFocus={this.focusMobNow.bind(this,'card')} maxLength='30'
                             ref='card' placeholder='请输入银行卡号' type="tel"/>
	                    <a className='blueClearInput' 
                            onClick={this.clearInput.bind(this,'card')} href="javascript:;">&times;</a>
                    </dd>
	                <dd><span>预留手机</span>
	                	<input onBlur={this.hideClearInput.bind(this,'mobile')} 
                         onFocus={this.focusMobNow.bind(this,'mobile')} maxLength='11' onChange={this.changeNum.bind(this,'mobile')} ref='mobile' placeholder='请输入您银行的预留号码' type="tel"/>
	                    <a className='blueClearInput' 
                            onClick={this.clearInput.bind(this,'mobile')} href="javascript:;">&times;</a>
                    </dd>
	                <dd><span>验证码</span>
                        <a ref='beforeSendCode' style={{display:(true?'none':'')}} 
                        onClick={this.beforeSendCode.bind(this)}
                            href="javascript:;" id='beforeSendCode'></a>
	                	<input maxLength='6' ref='code' placeholder='请输入验证码' type="tel"/>
                        <CountDown sendNow = {st.codeOk} time='60' />
                    </dd>
                </dl>
                <p><i>!</i>您的银行卡将用于后续放款、还款，绑定后无法进行修改</p>

                <a onClick={this.toSubmit.bind(this)} 
                    className={`btn_abs_bottom android_${this.state.anDroid}`} 
                    href="javascript:;">提交</a>
                <ModalSelectBank
                    isOpen={this.state.modalBankIsOpen}
                    onAfterOpen={this.openModalBank.bind(this)}
                    onRequestClose={this.closeModalBank.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height(),
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'#eee',
                            overflow:'visible',
                        }
                    }}
                >
                    <p className='cityTitle citylist rightSvg' 
                        onClick={this.closeModalBank.bind(this)}>
                        <i></i><label>请选择银行</label>
                        <Arrow fill='#3366cc'/>
                    </p>
                    <dl className='rightSvg bankList'>
                        {banks}
                        <dd></dd>
                    </dl>
                </ModalSelectBank>

                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>
                    {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                  </div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Bank, Reflux.connect(store,'m'));