import React,{Component} from  'react'
import Modal from 'react-modal'
import ModalImg from 'react-modal'
import ModalSelect from 'react-modal'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'
import Loading from '../../../components/Loading'
import _includes from 'lodash/includes'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Census'
import store from '../Stores/Census'

export default class Census extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            report:null,
            modalImgIsOpen:false,
            anDroid:_includes(navigator.appVersion,'Android')||navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger',
        }
        Actions.getUserextauths();
        localStorage.raiseRight = true;
    }
 
	componentDidMount() {
	    __event__.setHeader({
            title: '户籍证明',
            backBtn:true,
        });
	}


	openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        if(this.state.m.break){
            history.go(-1);
        }

        this.setState({
            modalIsOpen:false,
            errmsg:this.state.m.modalIsOpen?this.state.m.errorMsg:this.state.errmsg,
        });
        Actions.closeModal();
    }

    openModalImg(){
        this.setState({
            modalImgIsOpen:true
        })
    }

    closeModalImg() {
        this.setState({
            modalImgIsOpen:false
        })
    }

	myReport(){
		const file = this.refs.file;
		if(file.files.length){
            Actions.getSignature(this.refs.file.files);
		}
	}

    toSubmit(){
        const imageSend = this.state.m.imageSend;
        if(!imageSend){
            this.setState({
                modalIsOpen:true,
                errmsg:'请拍照上传您的户籍证明或户口本原件',
            });
        }else{
            Actions.census();
        }
    }

    render() {
    	const st = this.state.m;
       	return <section className='safety marry'>
                <dl className='operate'>
                    <dd>
                        <span>请拍照上传您的户籍证明或户口本原件</span>
                    </dd>
                	<dd>
                        <span>点击上传</span>
	                	<img onClick={this.openModalImg.bind(this)} 
                        style={{display:(st.imageSend?'':'none')}} 
                        src={st.imageSend} alt=""/>
	                	<input onChange={this.myReport.bind(this)} ref='file' id='file' type="file" accept="image/*"/>
	                	<a></a>
	                </dd>
                </dl>
                <a className={`btn_abs_bottom android_${this.state.anDroid}`}
                 href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>

                <ModalImg
                    isOpen={this.state.modalImgIsOpen}
                    onAfterOpen={this.openModalImg.bind(this)}
                    onRequestClose={this.closeModalImg.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            background: 'none',
                            border:0,
                            textAlign:'center',
                            display:'-webkit-flex',
                        }
                    }}
                >   
                    <a className='close' onClick={this.closeModalImg.bind(this)} href="javascript:;"></a>
                    <img src={st.imageSend}/>
                </ModalImg>

                <Modal
                    isOpen={st.modalIsOpen||this.state.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                    <div className='content'>
                        <i onClick={this.closeModal.bind(this)}>&times;</i>
                        {st.modalIsOpen?st.errorMsg:this.state.errmsg}
                    </div>
                    <div className='btn' onClick={this.closeModal.bind(this)}>
                        <a href="javascript:;">我知道了</a>
                    </div>
                </Modal>
                <Loading type={st.loading}/>
        </section>
    }
};

ReactMixin.onClass(Census, Reflux.connect(store,'m'));