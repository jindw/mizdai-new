import React,{Component} from  'react'
import Modal from 'react-modal'
import ModalImg from 'react-modal'
import ModalSelect from 'react-modal'
import Arrow from '../../../components/Header/Svg_Arrow_Left.jsx'

import ReactMixin from 'react-mixin'
import Reflux from 'reflux'
import Actions from '../Actions/Zhima'
import store from '../Stores/Zhima'

export default class Zhima extends Component {

	constructor(props) {
        super(props);
        this.state = {
            modalIsOpen:false,
            errmsg:'',
            report:null,
            modalImgIsOpen:false
        }
    }
 
	componentDidMount() {
	    __event__.setHeader({
            title: '芝麻信用',
            backBtn:true,
            raiseRight:true
        });
	}


	openModal(){
        this.setState({
            modalIsOpen:true
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen:false
        })
    }

    openModalImg(){
        this.setState({
            modalImgIsOpen:true
        })
    }

    closeModalImg() {
        this.setState({
            modalImgIsOpen:false
        })
    }

	myReport(){
		const file = this.refs.file;
		if(file.files.length){
			const reader = new FileReader();
			reader.readAsDataURL(file.files[0]);
			reader.onload = (e)=>{
				const [data,img] = [e.target.result,new Image()];
                img.src = data;
				this.setState({
					report:data,
                    reportBigger:img.width > $(window).width(),
				})
			}
            this.uploadImage();
		}
	}


    uploadImage(){
        const file = this.refs.file;
        const files = file.files;
        Actions.getSignature(files);
    }

    toSubmit(){
        const imageSend = this.state.m.imageSend;
        if(!imageSend){
            this.setState({
                modalIsOpen:true,
                errmsg:'请拍照上传您的芝麻信用分页面截图',
            });
        }else{
            
        }
    }

    render() {
    	const st = this.state;
       	return <section className='safety marry'>
                <dl className='operate'>
                    <dd>
                        <span>请拍照上传您的芝麻信用分页面截图</span>
                    </dd>
                	<dd>
                        <span>点击上传</span>
	                	<img onClick={this.openModalImg.bind(this)} style={{display:(this.state.report?'':'none')}} src={this.state.report} alt=""/>
	                	<input onChange={this.myReport.bind(this)} ref='file' id='file' type="file" accept="image/*"/>
	                	<a></a>
	                </dd>
                </dl>
                <p>注：芝麻信用分可通过“支付宝”-“我的”-“芝麻信用”进行查看</p>
                <a className='btn_abs_bottom' href="javascript:;" onClick={this.toSubmit.bind(this)}>提交</a>

                <ModalImg
                    isOpen={this.state.modalImgIsOpen}
                    onAfterOpen={this.openModalImg.bind(this)}
                    onRequestClose={this.closeModalImg.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            background: 'none',
                            border:0,
                            textAlign:'center',
                        }
                    }}
                >   
                    <i className='showBgPhoto' onClick={this.closeModalImg.bind(this)}
                        style={{
                            backgroundImage:`url(${this.state.report}`,
                            backgroundSize:(this.state.reportBigger?'contain':'')
                    }}></i>
                </ModalImg>

                <Modal
                    isOpen={st.modalIsOpen}
                    onAfterOpen={this.openModal.bind(this)}
                    onRequestClose={this.closeModal.bind(this)}
                    closeTimeoutMS={150}
                >
                  <div className='content'><i onClick={this.closeModal.bind(this)}>&times;</i>{st.errmsg}</div>
                  <div className='btn' onClick={this.closeModal.bind(this)}>
                      <a href="javascript:;">我知道了</a>
                  </div>
                </Modal>
        </section>
    }
};

ReactMixin.onClass(Zhima, Reflux.connect(store,'m'));