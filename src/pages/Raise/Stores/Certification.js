import Actions from '../Actions/Certification'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserauths(){
        this.data.loading = '';
        DB.Personal.getUserauths().then(data=>{
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            if(this.data.REALAUTH){
                // history.go(-1);
            }
        });
    },

    imgRemove(mFile){
        this.data.imgs[mFile] = '';
        this.trigger(this.data);
    },

    getSignature(file,mFile){
        this.data.loading = 'on';
        this.trigger(this.data);
        DB.UPYun.getSignature({upYunSaveFolder:'user'}).then(re=>{
            let data = new FormData();
            data.append('file', file.files[0]);
            data.append('policy', re.policy);
            data.append('signature', re.signature);
            $.ajax({
                url: '//v0.api.upyun.com/mizdai-static',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, 
                contentType: false,
                timeout: 30000,
            })
            .done(res => {
                this.data.imgs[mFile] = `//mizdai-static.b0.upaiyun.com${res.url}`;
            })
            .fail(data => {
                this.data.modalIsOpen = true;
                this.data.errorMsg = '操作失败，建议使用较小的图片';
            })
            .always(()=>{
                this.data.loading = '';
                this.trigger(this.data);
                file.value = '';
            });
        });
    },

    getRealNameAuth(realName,idCard){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Raise.getRealNameAuth({
            idCard,
            realName,
            idCardBackUrl:this.data.imgs.file_on,
            idCardFrontUrl:this.data.imgs.file_off,
            idCardHandUrl:this.data.imgs.file_hand,
        }).then(data=>{
            _assign(this.data,data);
            if(!data.result){
                this.data.errorMsg = `本次认证失败<br/>（可以再认证${3-data.realNameAuthFailCount}次）`;
                if(data.realNameAuthFailCount === 3){
                    this.data.errorMsg = '实名认证失败超过3次<br/>30天后可重新认证！';
                }
            }else{
                this.data.errorMsg = '认证成功';
                this.data.break = true;
            }
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        }, data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        })
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            break:false,
            loading:'',
            imgs:{
                file_on:'',
                file_off:'',
                file_hand:'',
            },
        };
        return this.data;
    }
});