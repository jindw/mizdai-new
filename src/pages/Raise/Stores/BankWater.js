import Actions from '../Actions/BankWater'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getBankList(){
    	DB.Raise.getBankList().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
    	},data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            bankInfos:[]
        };
        return this.data;
    }
});