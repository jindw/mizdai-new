import Actions from '../Actions/Zhima'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getSignature(files){
        this.data.loading = 'on';
        this.trigger(this.data);

        DB.UPYun.getSignature({upYunSaveFolder:'user'}).then(re=>{
            const data = new FormData();
            data.append('file', files[0]);
            data.append('policy', re.policy);
            data.append('signature', re.signature);
            $.ajax({
                url: '//v0.api.upyun.com/mizdai-static',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, 
                contentType: false,
            })
            .done(res=> {
                this.data.imageSend = `//mizdai-static.b0.upaiyun.com${res.url}`;
            })
            .fail(() =>{
                console.error("error");
            })
            .always(()=>{
                this.data.loading = '';
                this.trigger(this.data);
            });
        });
    },

    getInitialState() {
        this.data = {
            imageSend:null,
        };
        return this.data;
    }
});