import Actions from '../Actions/Car'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'

export default createStore({
    
    listenables: [Actions],

    getProvince(){
    	DB.Place.getPrivince().then(data=>{
    		this.data.provinces = data.provinces;
    		this.trigger(this.data);
    	});
    },

    getCity(province){
    	DB.Place.getCity({provinceCode:province.code}).then(data=>{
    		this.data.cities = data.cities;
    		this.data.sProvince = province.name;
    		this.trigger(this.data);
    	});
    },

    getInitialState() {
        this.data = {
            provinces:[],
            cities:[],
            sProvince:''
        };
        return this.data;
    }
});