import Actions from '../Actions/Bank'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserauths(){
        this.data.loading = '';
        DB.Personal.getUserauths().then(data=>{
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            if(!this.data.REALAUTH||this.data.BINDCARDAUTH){
                history.go(-1);
            }
        });
    },

    confirm(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.modalIsOpen = true;
        this.data.loading = '';
        DB.Banks.confirm(message).then(data=>{
            _assign(this.data,data);
            if(data.bindResult){
                this.data.errorMsg = '操作成功';
                this.data.break = true;
            }
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.codeOk = false;
            this.trigger(this.data);
        });
    },

    sendBindvalidateCode(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        DB.Banks.sendBindvalidateCode(message).then(data=>{
            _assign(this.data,data);
            this.data.codeOk = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.codeOk = false;
            this.trigger(this.data);
        });
    },

    getBankList(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
    	DB.Raise.getBankList().then(data=>{
            _assign(this.data,data);
            this.trigger(this.data);
    	},data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            bankInfos:[],
            loading:'',
        };
        return this.data;
    }
});