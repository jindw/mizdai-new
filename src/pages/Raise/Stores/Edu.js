import Actions from '../Actions/Edu'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'

export default createStore({
    
    listenables: [Actions],

    education(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalIsOpen = true;
        DB.Userextauths.education(_assign(message,{
            provinceCode:this.data.provinceCode,
        })).then(data=>{
            this.data.errorMsg = '提交成功';
            this.data.break = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    getProvince(){
    	DB.Place.getPrivince().then(data=>{
    		this.data.provinces = data.provinces;
    		this.trigger(this.data);
    	});
    },

    getCity(province){
        this.data.provinceCode = province.code;
    	DB.Place.getCity({provinceCode:province.code}).then(data=>{
    		this.data.cities = data.cities;
    		this.data.sProvince = province.name;
    		this.trigger(this.data);
    	});
    },

    getInitialState() {
        this.data = {
            provinces:[],
            cities:[],
            sProvince:''
        };
        return this.data;
    }
});