import Actions from '../Actions/Contact'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserauths(){
        this.data.loading = '';
        DB.Personal.getUserauths().then(data=>{
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            if(!this.data.REALAUTH||!this.data.BINDCARDAUTH||this.data.CONTACTSAUTH){
                history.go(-1);
            }
        });
    },

    addContacts(contacts){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
    	DB.Raise.addContacts({
            contacts:JSON.stringify(contacts),
        }).then(data=>{
            this.data.modalIsOpen = true;
            this.data.break = true;
            this.data.errorMsg = '操作成功 ';
            this.trigger(this.data);
    	},data=>{
            _assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            errmsg:'',
            break:false,
            modalWaitIsOpen:true,
            loading:''
        };
        return this.data;
    }
});