import Actions from '../Actions/Pact'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserauths(){
        this.data.loading = '';
        DB.Personal.getUserauths().then(data=>{
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            if(!this.data.REALAUTH||!this.data.BINDCARDAUTH||!this.data.MERCHANTAUTH||!this.data.CONTACTSAUTH||this.data.MERCHANTRENTAUTH){
                history.go(-1);
            }
        });
    },

    housecontfaractpics(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.modalIsOpen = true;
        this.data.loading = '';
        DB.Raise.housecontfaractpics({picUrls:this.data.myReport.toString()}).then(data=>{
            this.data.errorMsg = '操作成功';
            this.data.break = 'true';
            this.trigger(this.data);
        }, data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        })
    },

    removeImg(imgIndex){
        const myReport = this.data.myReport;
        myReport.splice(imgIndex,1);
        this.data.myReport = myReport;
        this.trigger(this.data);
    },

    getSignature(file){
        this.data.loading = 'on';
        this.trigger(this.data);

        DB.UPYun.getSignature({upYunSaveFolder:'user'}).then(re=>{
            const data = new FormData();
            data.append('file', file.files[0]);
            data.append('policy', re.policy);
            data.append('signature', re.signature);
            $.ajax({
                url: '//v0.api.upyun.com/mizdai-static',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, 
                contentType: false,
            })
            .done(res=> {
                this.data.imageSend = `//mizdai-static.b0.upaiyun.com${res.url}`;
                const myReport = this.data.myReport;
                myReport.splice(myReport.length,0,this.data.imageSend);
                this.data.myReport = myReport;
            })
            .fail(() =>{
                this.data.modalIsOpen = true;
                this.data.errorMsg = '操作失败';
            })
            .always(()=>{
                this.data.loading = '';
                this.trigger(this.data);
                file.value = '';
            });
        });
    },

    getInitialState() {
        this.data = {
            myReport:[],
            modalIsOpen:false,
        };
        return this.data;
    }
});