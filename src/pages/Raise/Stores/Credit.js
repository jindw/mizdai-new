import Actions from '../Actions/Credit'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserextauths(){
        DB.Personal.getUserextauths().then(data=>{
            this.data.loading = '';
            this.trigger(this.data);
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            if(this.data.PBCAUTH){
                history.go(-1);
            }
        });
    },

    pbc(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalIsOpen = true;
    	DB.Userextauths.pbc(message).then(data=>{
    		this.data.errorMsg = '提交成功';
            this.data.break = true;
    		this.trigger(this.data);
    	},data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            loading:'on',
            user:[],
        };
        return this.data;
    }
});