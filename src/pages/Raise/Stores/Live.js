import Actions from '../Actions/Live'
import Reflux,{createStore} from 'reflux'
import DB from '../../../app/db'
import _assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    getUserextauths(){
        DB.Personal.getUserextauths().then(data=>{
            this.data.loading = '';
            this.trigger(this.data);
            data.auths.forEach((itm,ind)=>this.data[itm.type] = +itm.status);
            if(this.data.LIVEAUTH){
                history.go(-1);
            }
        });
    },

    live(message){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.modalIsOpen = true;
        DB.Userextauths.live(_assign(message,{
            provinceCode:this.data.provinceCode,
            cityCode:this.data.cityCode,
            filePath:this.data.imageSend,
        })).then(data=>{
            this.data.errorMsg = '提交成功';
            this.data.break = true;
            this.trigger(this.data);
        },data=>{
            _assign(this.data,data);
            this.trigger(this.data);
        });
    },

    getSignature(files){
        this.data.loading = 'on';
        this.trigger(this.data);

        DB.UPYun.getSignature({upYunSaveFolder:'user'}).then(re=>{
            const data = new FormData();
            data.append('file', files[0]);
            data.append('policy', re.policy);
            data.append('signature', re.signature);
            $.ajax({
                url: '//v0.api.upyun.com/mizdai-static',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, 
                contentType: false,  
            })
            .done(res=> this.data.imageSend = `//mizdai-static.b0.upaiyun.com${res.url}`)
            .fail(() =>{
                this.data.modalIsOpen = true;
                this.data.errorMsg = '操作失败!';
            })
            .always(()=>{
                this.data.loading = '';
                this.trigger(this.data);
            });
        });
    },

    getProvince(){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
    	DB.Place.getPrivince().then(data=>{
    		this.data.provinces = data.provinces;
    		this.trigger(this.data);
    	});
    },

    getCity(province){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.provinceCode = province.code;
    	DB.Place.getCity({provinceCode:province.code}).then(data=>{
    		this.data.cities = data.cities;
    		this.data.sProvince = province.name;
            if(!data.cities.length){
                this.data.placeOver = true;
            }
    		this.trigger(this.data);
    	});
    },

    getCounty(city){
        this.data.loading = 'on';
        this.trigger(this.data);
        this.data.loading = '';
        this.data.cityCode = city.code;
    	DB.Place.getCounty({cityCode:city.code}).then(data=>{
    		this.data.county = data.districts;
    		this.data.sCity = city.name;
    		this.trigger(this.data);
    	});
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            title:'居住信息',
            provinces:[],
            cities:[],
            county:[],
            sProvince:'',
            sCity:''
        };
        return this.data;
    }
});