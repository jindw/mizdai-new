import Reflux,{createActions} from 'reflux'

export default createActions([
	'getUserauths',
	'applystatus',
	'getUserextauths',
	'closeModal',
	'getUserMsg',
	]);