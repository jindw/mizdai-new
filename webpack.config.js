var webpack = require('webpack')
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        apps: './src/app/app.jsx',
        common: [
            'react',
            'react-dom',
            'reflux',
            'react-mixin',
            'react-motion',
            'react-modal',
            'swiper',
            './node_modules/swiper/dist/css/swiper.css',
            'jquery',
            'cleave.js',
            './node_modules/cleave.js/dist/addons/cleave-phone.cn',
            'animate.css'
        ]
    },

    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js' //最终打包生产的文件名
    },

    devServer: {
        historyApiFallback: true,
        noInfo: true,
        inline: true
    },

    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                query: {
                    presets: ['es2015', 'react']
                }
            }, {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.(css|scss)$/,
                loader: "style-loader!css-loader!postcss-loader!sass-loader"
            }
        ]
    },

    // devtool: 'hidden-source-map',
    devtool: 'eval',
    watch: true,
    performance: {
        hints: false
    },
    target: 'web',
    plugins: [
      new HtmlWebpackPlugin({template: './index.html'}),

        // 开发环境配置
        new webpack.DefinePlugin({
            __LOCAL__: true, // 测试环境
            __PRO__: false, // 生产环境
        }),
        new HtmlWebpackPlugin({template: './index.html'}),
        new webpack.ProvidePlugin({
          Swiper: "swiper",
          '$':'jquery',
          Cleave:'cleave.js'
        }),
        new webpack.optimize.CommonsChunkPlugin({names: ['common'], minChunks: Infinity})
    ]

}
